<?php

use App\Models\Alert;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersAlertSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        factory(User::class, 5)->create()->each(function ($user) {
            $user->alerts()->save(factory(Alert::class)->make());
        });
    }
}
