<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFolderFile extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('folder_uploaded_file', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();

            // File belongs to many folders
            // Folders has many files
            $table->bigInteger('folder_id')->nullable(false)->unsigned()->index('ndx_folder_uploaded_file_folder_id');
            $table->foreign('folder_id', 'fk_folder_uploaded_file_folder_id')->references('id')->on('folders')->onDelete('cascade');
            $table->bigInteger('uploaded_file_id')->nullable(false)->unsigned()->index('ndx_folder_uploaded_file_uploaded_file_id');
            $table->foreign('uploaded_file_id', 'fk_folder_uploaded_file_uploaded_file_id')
                  ->references('id')
                  ->on('uploaded_files')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('folder_uploaded_file');
    }
}
