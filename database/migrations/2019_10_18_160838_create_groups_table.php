<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_emergency')->nullable(0)->default(0);
            $table->efficientUuid('uuid')->index();
            $table->string('name')->nullable(false);
            $table->timestamps();

            // Contacts belongs to user
            $table->bigInteger('user_id')->nullable(false)->unsigned()->index('ndx_group_user_id');
            $table->foreign('user_id', 'fk_group_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
