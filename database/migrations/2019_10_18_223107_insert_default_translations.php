<?php

use App\Models\Translation;
use Illuminate\Database\Migrations\Migration;

class InsertDefaultTranslations extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        foreach ($this->getDefaultTranslations() as $security_question) {
            Translation::firstOrCreate(
                [
                    'locale' => $security_question['locale'],
                    'key'    => $security_question['key']
                ],
                [
                    'translated'    => $security_question['translated'],
                    'created_by'    => $security_question['created_by']
                ]
            );
        }
    }

    public function getDefaultTranslations(): array
    {
        return [
            [
                'locale'        => 'en_US',
                'key'           => 'file.types.local',
                'translated'    => 'Local file',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'file.types.local',
                'translated'    => 'Fichiers local',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'file.types.external',
                'translated'    => 'External file',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'file.types.external',
                'translated'    => 'Fichier externe',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'question.favorite-book',
                'translated'    => 'What is your favorite book?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'question.favorite-book',
                'translated'    => 'Quel est votre livre favori?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'question.road-grew-up',
                'translated'    => 'What is the name of the road you grew up on?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'question.road-grew-up',
                'translated'    => 'Quel est le nom de la route sur laquelle vous avez grandi?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'question.mothers-maiden-name',
                'translated'    => 'What is your mother’s maiden name?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'question.mothers-maiden-name',
                'translated'    => 'Quel est le nom de jeune fille de votre mère?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'question.favorite-pet',
                'translated'    => 'What was the name of your first/current/favorite pet?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'question.favorite-pet',
                'translated'    => 'Quel était le nom de votre premier animal de compagnie / actuel / préféré?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'question.first-company',
                'translated'    => 'What was the first company that you worked for?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'question.first-company',
                'translated'    => 'Quelle a été la première entreprise pour laquelle vous avez travaillé?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'question.meet-spouse',
                'translated'    => 'Where did you meet your better half?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'question.meet-spouse',
                'translated'    => 'Où avez-vous rencontré votre meilleure moitié?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'question.favorite-food',
                'translated'    => 'What is your favorite food?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'question.favorite-food',
                'translated'    => 'Quel est votre plat préféré?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'question.favorite-vacation',
                'translated'    => 'Where is your favorite place to vacation?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'question.favorite-vacation',
                'translated'    => 'Où est ton endroit préféré pour les vacances?',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'alert.types.intrusion',
                'translated'    => 'Intrusion',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'alert.types.intrusion',
                'translated'    => 'Intrusion',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'alert.types.emergency',
                'translated'    => 'Emergency',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'alert.types.emergency',
                'translated'    => 'Urgence',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'alert.types.programmed',
                'translated'    => 'Alerte programmé',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'alert.types.programmed',
                'translated'    => 'Alerte programmé',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'alert.types.blackmail',
                'translated'    => 'Blackmail Alert',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'alert.types.blackmail',
                'translated'    => 'Alerte "Chantage"',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'alert.types.intrusion.description',
                'translated'    => 'If someone try to access your account',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'alert.types.intrusion.description',
                'translated'    => 'Si quelqu\'un essaye d\'acceder à votre compte',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'alert.types.emergency.description',
                'translated'    => 'Emergency alert sent immediatly to your emergency contact',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'alert.types.emergency.description',
                'translated'    => 'Message d\'urgence envoyé directement à votre contact d\'urgence',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'alert.types.programmed.description',
                'translated'    => 'Planned alert, you must set a time to send the alert',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'alert.types.programmed.description',
                'translated'    => 'Alerte planifié, choisissez à quel heure l\'alerte sera envoyé si vous ne la desactivez pas',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'alert.types.blackmail.description',
                'translated'    => 'Blackmail mode let you create an alert who will be sent if you don\'t disable an alarm set in an inteval',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'alert.types.blackmail.description',
                'translated'    => 'Le mode "chantage" vous permet de creer un alerte qui envarra une notifcation chronique et enverra l\'alerte si vous ne la desactivez pas',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'civility.mr',
                'translated'    => 'Mr',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'civility.mr',
                'translated'    => 'Mr',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'civility.mrs',
                'translated'    => 'Mrs',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'civility.mrs',
                'translated'    => 'Mme',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'en_US',
                'key'           => 'civility.other',
                'translated'    => 'Other',
                'created_by'    => 'Migration'
            ],
            [
                'locale'        => 'fr_FR',
                'key'           => 'civility.other',
                'translated'    => 'Autre',
                'created_by'    => 'Migration'
            ],
        ];
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        foreach ($this->getDefaultTranslations() as $security_question) {
            Translation::where([
                'locale' => $security_question['locale'],
                'key'    => $security_question['key']
                ])->delete();
        }
    }
}
