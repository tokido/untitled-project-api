<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertNotificationTimesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('alert_notification_times', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->datetime('notify_at')->nullable(false);
            $table->boolean('is_recurrent')->nullable(false)->default(1);
            $table->timestamps();
            $table->softDeletes();

            // Belongs to an alert
            $table->bigInteger('alert_id')->nullable(false)->unsigned()->index('ndx_alert_notification_time_alert_id');
            $table->foreign('alert_id', 'fk_alert_notification_time_alert_id')->references('id')->on('alerts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('alert_notification_times');
    }
}
