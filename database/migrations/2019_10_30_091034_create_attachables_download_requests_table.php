<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttachablesDownloadRequestsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        // @TODO
        Schema::create('contact_attachable_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->efficientUuid('uuid')->index();
            $table->boolean('is_download_ready')->nullable(false)->default(0);
            // The ID the contact have to use to request the file
            $table->string('random_id')->nullable(false)->unique();
            $table->string('random_password')->nullable(false);
            $table->datetime('requested_at')->nullable();
            $table->datetime('ready_at')->nullable();
            $table->datetime('expires_at')->nullable();
            $table->datetime('downloaded_at')->nullable();
            $table->integer('request_count')->nullable(false)->default(0);
            $table->integer('download_count')->nullable(false)->default(0);
            $table->timestamps();

            $table->bigInteger('contact_id')->nullable(false)->unsigned()->index('ndx_attachable_download_contact_id');
            $table->foreign('contact_id', 'fk_attachable_download_contact_id')->references('id')->on('contacts')->onDelete('cascade');

            $table->bigInteger('alert_id')->nullable(false)->unsigned()->index('ndx_attachable_request_alert_id');
            $table->foreign('alert_id', 'fk_attachable_request_alert_id')->references('id')->on('alerts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('contact_attachable_requests');
    }
}
