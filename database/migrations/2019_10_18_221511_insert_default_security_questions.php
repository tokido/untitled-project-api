<?php

use App\Models\SecurityQuestion;
use Illuminate\Database\Migrations\Migration;

class InsertDefaultSecurityQuestions extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        foreach ($this->getDefaultSecurityQuestions() as $security_question) {
            SecurityQuestion::firstOrCreate(
                [
                    'label' => $security_question['label']
                ],
                [
                    'is_enabled'    => $security_question['is_enabled']
                ]
            );
        }
    }

    /**
     * get default security questions.
     */
    public function getDefaultSecurityQuestions(): array
    {
        return config('global.password.two-factor-recovery.questions');
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        foreach ($this->getDefaultSecurityQuestions() as $security_question) {
            SecurityQuestion::where(['label' => $security_question['label']])->delete();
        }
    }
}
