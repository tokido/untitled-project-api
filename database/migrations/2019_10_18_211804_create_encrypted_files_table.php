<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEncryptedFilesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('encrypted_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->efficientUuid('uuid')->index();
            $table->string('path')->nullable(false);
            $table->string('name')->nullable(false);
            $table->integer('block')->nullable(false)->default(1);
            $table->integer('size')->nullable(false);
            $table->softDeletes();
            $table->timestamps();

            // File has one to many encrypted Parts
            $table->bigInteger('uploaded_file_id')->nullable(false)->unsigned()->index('ndx_encrypted_file_uploaded_file_id');
            $table->foreign('uploaded_file_id', 'fk_encrypted_file_uploaded_file_id')
                  ->references('id')
                  ->on('uploaded_files')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('encrypted_files');
    }
}
