<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertablesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('alertables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            // Morph to many has alerts
            $table->bigInteger('alert_id')->nullable(false)->unsigned()->index('ndx_alertables_alert_id');
            $table->foreign('alert_id', 'fk_alertables_alert_id')->references('id')->on('alerts')->onDelete('cascade');
            // Morphable field
            $table->morphs('alertable');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('alertables');
    }
}
