<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoldersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('folders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->efficientUuid('uuid')->index();
            $table->string('name')->nullable(false);
            $table->timestamps();
            $table->softDeletes();

            // Folder belongs to user
            $table->bigInteger('user_id')->nullable(false)->unsigned()->index('ndx_folder_user_id');
            $table->foreign('user_id', 'fk_folder_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('folders');
    }
}
