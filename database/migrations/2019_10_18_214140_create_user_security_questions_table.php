<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSecurityQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('user_security_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->efficientUuid('uuid')->index();
            $table->string('answer')->nullable(false);
            $table->string('tip')->nullable();
            $table->timestamps();

            // Answer belongs to user
            $table->bigInteger('user_id')->nullable(false)->unsigned()->index('ndx_user_security_question_user_id')->unique();
            $table->foreign('user_id', 'fk_user_security_question_user_id')->references('id')->on('users')->onDelete('cascade');
            // answer belongs to one security question
            $table->bigInteger('security_question_id')
                  ->nullable(false)
                  ->unsigned()
                  ->index('ndx_user_security_question_security_question_id');
            $table->foreign('security_question_id', 'fk_user_security_question_security_question_id')
                  ->references('id')
                  ->on('security_questions')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('user_security_questions');
    }
}
