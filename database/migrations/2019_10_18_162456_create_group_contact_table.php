<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupContactTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('group_contact', function (Blueprint $table) {
            $table->bigIncrements('id');

            // Contacts belongs to many group
            // Group has many contacts
            $table->bigInteger('group_id')->nullable(false)->unsigned()->index('ndx_group_group_contact_id');
            $table->foreign('group_id', 'fk_group_group_contact_id')->references('id')->on('groups')->onDelete('cascade');
            $table->bigInteger('contact_id')->nullable(false)->unsigned()->index('ndx_contact_group_contact_id');
            $table->foreign('contact_id', 'fk_contact_group_contact_id')->references('id')->on('contacts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('group_contact');
    }
}
