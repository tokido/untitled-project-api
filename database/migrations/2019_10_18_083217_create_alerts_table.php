<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->efficientUuid('uuid')->index();
            $table->string('title')->nullable(false)->default('default');
            $table->text('message')->nullable(false);
            $table->integer('notification_interval')->nullable();
            $table->string('location_coordinates', 30)->nullable();
            $table->boolean('is_draft')->nullable(false)->default(0);
            $table->boolean('is_enabled')->nullable(false)->default(1);
            $table->boolean('is_suspended')->nullable(false)->default(0);
            $table->boolean('is_sent')->nullable(false)->default(0);
            $table->boolean('is_download_file_ready')->nullable(false)->default(false);

            $table->dateTime('sent_at')->nullable();
            $table->dateTime('programmed_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // Alert belongs to user
            $table->bigInteger('user_id')->nullable(false)->unsigned()->index('ndx_alert_user_id');
            $table->foreign('user_id', 'fk_alert_user_id')->references('id')->on('users')->onDelete('cascade');

            // Alert belongs to AlertType
            $table->bigInteger('alert_type_id')->nullable(false)->unsigned()->index('ndx_alert_alert_type_id');
            $table->foreign('alert_type_id', 'fk_alert_alert_type_id')->references('id')->on('alert_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('alerts');
    }
}
