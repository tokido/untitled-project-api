<?php

use App\Models\UploadedFileType;
use Illuminate\Database\Migrations\Migration;

class InsertUpdoadedFileTypes extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        foreach ($this->getDefaultUploadedFileTypes() as $file_type) {
            UploadedFileType::firstOrCreate(
                [
                    'type' => $file_type['type']
                ],
                [
                    'is_default'    => $file_type['is_default'],
                    'is_enabled'    => $file_type['is_enabled']
                ]
            );
        }
    }

    public function getDefaultUploadedFileTypes(): array
    {
        return config('global.files.types');
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        foreach ($this->getDefaultUploadedFileTypes() as $file_type) {
            UploadedFileType::where(['type' => $file_type['type']])->delete();
        }
    }
}
