<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->efficientUuid('uuid')->index();
            $table->enum('notification_from', ['interval', 'times', 'ocurrences'])->nullable(false);
            $table->enum('disabled_from', ['mobile', 'web', 'admin', 'other'])->nullable();
            $table->enum('sent_to', ['mobile', 'web', 'both'])->nullable(false);
            $table->datetime('sent_at')->nullable(false);
            $table->datetime('expires_at')->nullable(false);
            $table->datetime('disabled_at')->nullable();
            $table->boolean('is_waiting_response')->nullable(false)->default(true);
            $table->boolean('was_disabled')->nullable(false)->default(false);
            $table->integer('ocurrence_left')->nullable();
            $table->timestamps();

            // To the owner
            $table->bigInteger('user_id')->nullable(false)->unsigned()->index('ndx_notification_user_id');
            $table->foreign('user_id', 'fk_notification_user_id')->references('id')->on('users')->onDelete('cascade');

            // To alert
            $table->bigInteger('alert_id')->nullable(false)->unsigned()->index('ndx_notification_alert_id');
            $table->foreign('alert_id', 'fk_notification_alert_id')->references('id')->on('alerts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
