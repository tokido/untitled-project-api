<?php

use App\Models\AlertType;
use Illuminate\Database\Migrations\Migration;

class InsertDefaultAlertTypes extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        foreach ($this->getDefaultAlertTypes() as $alert_type) {
            AlertType::firstOrCreate(
                ['name'          => $alert_type['name']],
                [
                    'description'   => $alert_type['description'],
                    'is_deletable'  => $alert_type['is_deletable'],
                    'is_creatable'  => $alert_type['is_creatable'],
                ]
            );
        }
    }

    public function getDefaultAlertTypes(): array
    {
        return config('global.alert.types');
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        foreach ($this->getDefaultAlertTypes() as $alert_type) {
            AlertType::where(['name' => $alert_type['name']])->delete();
        }
    }
}
