<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadedFilesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('uploaded_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->efficientUuid('uuid')->index();
            $table->string('original_name')->nullable(false);
            $table->string('generated_name')->nullable(false);
            $table->string('path')->nullable(false);
            $table->string('extension')->nullable(false);
            $table->string('mime_type')->nullable();
            $table->boolean('is_encryption_success')->nullable(false)->default(false);
            $table->boolean('is_decryption_success')->nullable();
            $table->datetime('decryption_start')->nullable();
            $table->datetime('encryption_start')->nullable();
            $table->datetime('encryption_end')->nullable();
            $table->datetime('decryption_end')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // Uploaded files belong to a upload type
            $table->bigInteger('uploaded_file_type_id')->nullable(false)->unsigned()->index('ndx_uploaded_file_uploaded_file_type_id');
            $table->foreign('uploaded_file_type_id', 'fk_uploaded_file_uploaded_file_type_id')
                  ->references('id')
                  ->on('uploaded_file_types')
                  ->onDelete('cascade');

            // File belongs to user
            $table->bigInteger('user_id')->nullable(false)->unsigned()->index('ndx_uploaded_file_user_id');
            $table->foreign('user_id', 'fk_uploaded_file_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('uploaded_files');
    }
}
