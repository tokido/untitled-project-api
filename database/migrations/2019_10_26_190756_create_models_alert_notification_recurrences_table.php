<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelsAlertNotificationRecurrencesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('alert_notification_recurrences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('day_of_week', [0, 1, 2, 3, 4, 5, 6])->nullable(false)->default(1);
            $table->string('hour')->nullable(false)->default('08:00:00');
            $table->boolean('is_recurrent')->nullable(false)->default(1);
            $table->timestamps();
            $table->softDeletes();

            // Belongs to an alert
            $table->bigInteger('alert_id')->nullable(false)->unsigned()->index('ndx_alert_notification_recurrence_alert_id');
            $table->foreign('alert_id', 'fk_alert_notification_recurrence_alert_id')->references('id')->on('alerts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('alert_notification_recurrences');
    }
}
