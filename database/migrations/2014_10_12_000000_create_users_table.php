<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->efficientUuid('uuid')->index();
            $table->string('name');
            $table->string('civility')->nullable(false)->default('civility.other');
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone')->unique()->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->timestamp('verified_at')->nullable();
            $table->integer('first_auth_steps')->nullable(false)->default(0);
            $table->boolean('restricted_mode')->nullable(false)->default(0);
            $table->string('verification_number')->nullable();
            $table->string('password');
            $table->string('pin')->nullable();
            $table->string('locale', 10)->nullable(false)->default('en_US');
            $table->string('country', 2)->nullable(true);
            $table->boolean('use_receipt')->nullable(false)->default(1);
            $table->boolean('use_pin')->nullable(false)->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
