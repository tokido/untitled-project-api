<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name'              => $faker->name,
        'email'             => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token'    => Str::random(10),
        'use_pin'           => rand(0, 1),
        'use_receipt'       => rand(0, 1),
        'country'           => Countries::getOne('FR', 'EN'),
        'locale'            => Miscellaneous::getOne('fr_FR', 'en_EN'),
        'pin'               => str_pad(rand(0, pow(10, 4) - 1), 4, '0', STR_PAD_LEFT),
        'phone'             => $faker->unique()->e164PhoneNumber,
        'civility'          => config('global.users.civility')[array_rand(config('global.users.civility'))],
        'phone_verified_at' => now()
    ];
});
