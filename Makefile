.PHONY: help build up run tests
EXEC_PHP   = php
SYMFONY    = $(EXEC_PHP) bin/console
DOCKER_COMPOSE     = docker-compose
.DEFAULT_GOAL := help

help:
	@echo ""
	@echo "usage: make COMMAND"
	@echo ""
	@echo "Quick-start Commands:"
	@echo " docker.update-deps          Update docker image dependencies"
	@echo " start                Create and start containers"
	@echo " start-wlogs          Create and start containers with logs in console"
	@echo " stop                 Stop containers"
	@echo " "

## Start api-platform ecosystem
start:
	@$(DOCKER_COMPOSE) up -d

## Start api-platform ecosystem with log
start-wlogs:
	@$(DOCKER_COMPOSE) up

## Stop api-platform
stop:
	@$(DOCKER_COMPOSE) down

## Console api-platform
php.console:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) /bin/bash

## Migrate database
migrate:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) php artisan migrate

## Lint
lint.cs:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) ./vendor/bin/phpcs -s

## Lint .cbf
lint.cbf:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) ./vendor/bin/phpcbf;

## Fixer
lint.fixer:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) ./vendor/bin/php-cs-fixer fix --diff -v

## Duplication checker
lint.dup:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) ./vendor/bin/phpcpd --fuzzy --progress ./app ./config ./resources ./database ./routes

## Larastan
lint.stan:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) php artisan code:analyse

## Fix
lint.fix: lint.cbf | lint.fixer

## Lint :
lint: lint.cs | lint.fixer

## Seed :
seed:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) php artisan db:seed

## Dump autoload
dump-autoload:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) php composer.phar dump-autoload

## Lint :
dump.and.seed: dump-autoload | seed

## Testing
testing.migrate:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) php artisan migrate --env=testing

## Run Cron on the foreground
cron:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) cron -f

## Run worker
queue.work:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) php artisan queue:work

## Run worker
apidocs:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) php artisan apidoc:generate --force

## Run passport install
passport.install:
	@$(DOCKER_COMPOSE) exec $(EXEC_PHP) php artisan passport:install --force
