# Application readme

## Docker

    - Do not forget to update docker/mysql/testing-database.sql with your own access configuration 

## Redis

    - Redis is configured as defaut, suggests Redsmin for GUI
    - To use redsmin : 
        -  npm install redsmin --global
        -  $env:REDSMIN_KEY="5dac1e7aa9727b06d5400233"
        - redsmin

## Installation

    - composer install
    - php artisan migrate
    - php artisan passport:keys
    - php artisan passport:install
    - install cron

## Api doc

    - api doc is accessible from dev envivornment only and you must generate it yourself with the command : php artisan apidoc:generate --force

## Mailcatcher en dev

    - APP_URL:1080
