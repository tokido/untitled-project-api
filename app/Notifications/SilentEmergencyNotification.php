<?php

namespace App\Notifications;

use App\Models\Alert;
use App\Models\Contact;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;

class SilentEmergencyNotification extends Notification
{
    use Queueable;

    protected $contact;

    protected $alert;

    protected $user;

    /**
     * Create a new notification instance.
     */
    public function __construct(Contact $contact, Alert $alert, User $user)
    {
        $this->contact = $contact;
        $this->alert = $alert;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return null !== $this->contact->phone ? ['nexmo'] : ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->greeting('Hello' . $this->contact->name . ',')
            ->line($this->user->name . '\'s account was hacked or he is under pressure to open his account.')
            ->line('please take action about this notification. call your friend and call the police.')
            ->line('the A team.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
        ];
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return NexmoMessage
     */
    public function toNexmo($notifiable)
    {
        return (new NexmoMessage())
            ->content('Emergency message from : ' . $this->user->name);
    }

    public function routeNotificationForNexmo($notification)
    {
        return $this->phone;
    }
}
