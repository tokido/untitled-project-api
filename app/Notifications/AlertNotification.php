<?php

namespace App\Notifications;

use App\Models\Alert;
use App\Models\Contact;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;

class AlertNotification extends Notification
{
    use Queueable;

    protected $attachableRequest;
    protected $contact;
    protected $alert;
    protected $user;

    /**
     * Create a new notification instance.
     */
    public function __construct(Contact $contact, Alert $alert, User $user, $attachableRequest)
    {
        $this->contact = $contact;
        $this->alert = $alert;
        $this->user = $user;
        $this->attachableRequest = $attachableRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return null !== $this->contact->phone ? ['nexmo'] : ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $attachableLine = '';
        $dowloadLine = '';
        if (null !== $this->attachableRequest) {
            $attachableLine = 'username: ' . $this->attachableRequest->random_id . '. ';
            $attachableLine .= 'password: ' . decrypt($this->attachableRequest->random_password) . ' ';
            $dowloadLine = 'You can download files attached to this alert from : ' . config('app.url');
        }
        /*
         * @Todo : add attachments link request to the notifiable
         */
        return (new MailMessage())
            ->greeting('Hello ' . $this->contact->name . ',')
            ->line('This is a message from ' . $this->user->name . ' : ')
            ->line($this->alert->message)
            ->line($dowloadLine)
            ->line($attachableLine)
            ->line('the A team. For more informations : Link here');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
        ];
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return NexmoMessage
     */
    public function toNexmo($notifiable)
    {
        $attachableLine = '';
        $dowloadLine = '';
        if (null !== $this->attachableRequest) {
            $attachableLine = 'username: ' . $this->attachableRequest->random_id . '. ';
            $attachableLine .= 'password: ' . decrypt($this->attachableRequest->random_password) . ' ';
            $dowloadLine = 'You can download files he attached to this alert from : ' . config('app.url');
        }

        return (new NexmoMessage())
            ->content(
                'Hello ' . $this->contact->name . ',
                Message from : ' . $this->user->name . ' : ' .
                $this->alert->message . ' ' .
                $dowloadLine . ' ' .
                $attachableLine . ' '
            );
    }
}
