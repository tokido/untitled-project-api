<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;

class VerifyUserContact extends Notification implements ShouldQueue
{
    use Queueable;

    const EMAIL = 'email';
    const PHONE = 'phone';

    protected $user;

    protected $field;

    /**
     * Create a new notification instance.
     */
    public function __construct(User $user, string $field)
    {
        $this->user = $user;
        $this->field = $field;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return self::EMAIL === $this->field ? ['mail'] : ['nexmo'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->greeting('Hello!' . $this->user->name)
            ->line('Please validate your ' . $this->field . ' with the code : ' . show_pin($this->user->verification_number))
            ->line('The code will be valid for 5 minutes');
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return NexmoMessage
     */
    public function toNexmo($notifiable)
    {
        return (new NexmoMessage())
            ->content('Please validate your ' . $this->field . ' : ' . show_pin($this->user->verification_number) .
            ' The code will be valid for 5 minutes. The saver team. ');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
        ];
    }
}
