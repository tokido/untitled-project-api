<?php

namespace App\Jobs;

use App\Models\User;
use App\Notifications\SilentEmergencyNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessSilentEmergency implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $userUuid;

    /**
     * Create a new job instance.
     */
    public function __construct(string $userUuid)
    {
        $this->userUuid = $userUuid;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $user = User::whereUuid($this->userUuid)->first();

        if (null !== $user) {
            $emergencyType = AlertType::where(['name' => config('global.alert.emergency.type')])->first();
            $emergencies = Alert::where('alert_type_id', $emergencyType->id)->get();
            foreach ($emergencies as $alert) {
                // Notify All emergency contacts
                $emergencyGroups = $alert->groups()->get();
                if (null !== $emergencyGroups && \count($emergencyGroups) > 0) {
                    foreach ($emergencyGroups as $group) {
                        $groupMembers = $group->contacts;
                        if (null !== $groupMembers && \count($groupMembers) > 0) {
                            foreach ($groupMembers as $contact) {
                                $contact->notify(new SilentEmergencyNotification($contact, $alert, $user));
                            }
                        }
                    }
                }
                $emergencyContacts = $alert->contacts()->get();
                if (null !== $emergencyContacts && \count($emergencyContacts) > 0) {
                    foreach ($emergencyContacts as $contact) {
                        $contact->notify(new SilentEmergencyNotification($contact, $alert, $user));
                    }
                }
            }
        }
    }
}
