<?php

namespace App\Jobs;

use App\Models\Alert;
use App\Models\AlertType;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AlertBlackmailNotificationListener implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $this->checkNotificationsToSend();
    }

    public function checkNotificationsToSend()
    {
        // Run programmed alert first
        $typeHasRecurrences = config('global.alert.has_recurrences');

        // Marge sur les temps de notification
        $delta = 60;
        $now = Carbon::now();

        $alertTypeWithRecurrence = AlertType::where(['name' => $typeHasRecurrences])->first();

        if (null === $alertTypeWithRecurrence) {
            AdminNotifier::dispatch(
                'alert',
                trans('notifications.blackmail.not-found'),
                'AlertType'
            )->onQueue('default');

            return;
        }
        // Check has interval firsts
        // Alerts With intervals
        $enabledAlertsWithRecurrence = Alert::where([
            'is_draft'      => false,
            'is_enabled'    => true,
            'is_sent'       => false,
            'is_suspended'  => false,
            'alert_type_id' => $alertTypeWithRecurrence->id
        ])->with(['alertNotificationRecurrences'])->get();

        // verify notification interval or notification times
        if (\count($enabledAlertsWithRecurrence) > 0) {
            foreach ($enabledAlertsWithRecurrence as $enabledAlert) {
                if (\count($enabledAlert->alertNotificationRecurrences) > 0) {
                    foreach ($enabledAlert->alertNotificationRecurrences as $notificationRecurrence) {
                        if ((int) $now->dayOfWeek === (int) $notificationRecurrence->day_of_week) {
                            $currentDate = $now->format('Y-m-d');
                            $currentNotification = Carbon::createFromFormat('Y-m-d H:i:s', $currentDate . ' ' . $notificationRecurrence->hour);
                            $diffFromNotif = $currentNotification->diffInSeconds($now);
                            if ($diffFromNotif <= $delta) {
                                // Send notifications and save to database
                                $notification = new Notification();
                                $notification->createEntry($enabledAlert, 'ocurrences');
                                \Log::info('send Notifications :' . $enabledAlert->uuid);
                            }
                        }
                    }
                }
            }
        }
    }
}
