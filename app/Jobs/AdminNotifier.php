<?php

namespace App\Jobs;

use App\Mail\AdminErrorReport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class AdminNotifier implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $process;

    protected $message;

    protected $class;

    protected $uuid;

    /**
     * Create a new job instance.
     */
    public function __construct(string $process, string $message, $class = null, $uuid = null)
    {
        $this->process = $process;
        $this->message = $message;
        $this->class = $class;
        $this->uuid = $uuid;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        Mail::to(config('global.admin.email'))
            ->send(new AdminErrorReport(
                $this->process,
                $this->message,
                $this->class,
                $this->uuid
            ));
    }
}
