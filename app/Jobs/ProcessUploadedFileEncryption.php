<?php

namespace App\Jobs;

use App\Models\UploadedFile;
use FileEncryption;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessUploadedFileEncryption implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $uploadedFileUuid;

    /**
     * Create a new job instance.
     */
    public function __construct(string $uploadedFileUuid)
    {
        $this->uploadedFileUuid = $uploadedFileUuid;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $uploadedFile = UploadedFile::whereUuid($this->uploadedFileUuid)->first();
        if (null !== $uploadedFile) {
            FileEncryption::encrypt($uploadedFile);
        }
    }
}
