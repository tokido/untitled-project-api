<?php

namespace App\Jobs;

use App\Contracts\Repositories\AlertRepositoryInterface;
use App\Models\Alert;
use App\Models\ContactAttachableRequest;
use App\Notifications\NotifyFileDownloadReady;
use FileEncryption;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;

class PrepareAttachableDownload implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $alertUuid;

    protected $attachableRequestUuid;

    /**
     * Create a new job instance.
     *
     * @param string $alertUuid
     * @param string $attachableRequestUuid
     */
    public function __construct(string $alertUuid, string $attachableRequestUuid)
    {
        $this->alertUuid = $alertUuid;
        $this->attachableRequestUuid = $attachableRequestUuid;
    }

    /**
     * Execute the job.
     */
    public function handle(AlertRepositoryInterface $alertRepository)
    {
        $this->createDownloadableZip($alertRepository);
    }

    /**
     * Decrypt attachables, zip & downoad.
     */
    protected function createDownloadableZip($alertRepository): void
    {
        $alert = Alert::whereUuid($this->alertUuid)->first();
        $downloadable = $alertRepository->requestDownloadAttachables($alert);
        $attachableRequest = ContactAttachableRequest::whereUuid($this->attachableRequestUuid)->first();
        // Destination
        $zipDirectory = config('global.files.paths.temp') . $alert->uuid . '/';
        // Create directory

        if (!is_dir($zipDirectory)) {
            mkdir($zipDirectory, 0700, true);
        }
        $zipName = $alert->getZipPath();

        if (is_dir($zipDirectory)) {
            if (!file_exists($zipName)) {
                // Recreate all file
                if ($downloadable->has('files')) {
                    foreach ($downloadable->get('files') as $file) {
                        $fileName = $zipDirectory . $file->original_name;
                        FileEncryption::decrypt($file, $fileName);
                    }
                }
                // Recreate all folder
                if ($downloadable->has('folders')) {
                    foreach ($downloadable->get('folders') as $folderName => $filesFromFolder) {
                        // Create virtual Directory
                        $folderDirectory = $zipDirectory . $folderName . '/';
                        if (!is_dir($folderDirectory)) {
                            mkdir($folderDirectory, 0700, true);
                        }
                        if (is_dir($folderDirectory)) {
                            foreach ($filesFromFolder as $downloadableFileFromFolder) {
                                $folderFileName = $folderDirectory . $downloadableFileFromFolder->original_name;
                                FileEncryption::decrypt($downloadableFileFromFolder, $folderFileName);
                            }
                        }
                    }
                }
                // zip files
                $zipName = $alert->getZipPath();

                $this->zipAlertFolder($zipDirectory, $zipName);
                custom_remove_dir($zipDirectory);
            }

            $attachableRequest->setContactRequestDowloadReady();
            $attachableRequest->contact->notify(new NotifyFileDownloadReady($attachableRequest->contact));
            $alert->is_download_file_ready = true;
            $alert->save();
        }
    }

    protected function zipAlertFolder($source, $destination)
    {
        if (!\extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (true === is_dir($source)) {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file) {
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if (\in_array(mb_substr($file, mb_strrpos($file, '/') + 1), ['.', '..'], true)) {
                    continue;
                }

                $file = realpath($file);

                if (true === is_dir($file)) {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                } elseif (true === is_file($file)) {
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        } elseif (true === is_file($source)) {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();
    }
}
