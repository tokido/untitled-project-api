<?php

namespace App\Jobs;

use App\Models\Alert;
use App\Models\ContactAttachableRequest;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WatchExpiredZipFiles implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $now = Carbon::now();
        //Get zip ready groupBy Alert
        $zippedFiles = ContactAttachableRequest::whereNotNull('ready_at')
            ->whereNotNull('expires_at')
            ->where(['is_download_ready' => true])
            ->get();

        $groupedByAlerts = $zippedFiles->groupBy('alert_id');

        //turn around key values :
        if (\count($groupedByAlerts) > 0) {
            foreach ($groupedByAlerts as $alert_id => $attachableRequests) {
                $delete_zip_file = true;
                foreach ($attachableRequests as $attachableRequest) {
                    if (false === $attachableRequest->expires_at->greaterThan($now)) {
                        $delete_zip_file = false;
                    }
                }
                if (true === $delete_zip_file) {
                    $alert = Alert::find($alert_id);
                    $zipName = $alert->getZipPath();
                    if (file_exists($zipName)) {
                        unlink($zipName);
                    }
                    // restore all atachable request states
                    foreach ($attachableRequests as $attachableRequest) {
                        $attachableRequest->restorePristineStatus();
                    }
                }
            }
        }
    }
}
