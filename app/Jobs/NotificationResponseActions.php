<?php

namespace App\Jobs;

use App\Contracts\Repositories\AlertRepositoryInterface;
use App\Contracts\Repositories\NotificationRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotificationResponseActions implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     */
    public function handle(
        NotificationRepositoryInterface $notificationRepository,
        AlertRepositoryInterface $alertRepository
    ) {
        $enabledNotifications = $notificationRepository->getEnabledNotifications();
        \Log::info('NotificationActionResponse');
        \Log::info($enabledNotifications);

        // Notifications that hasn't been disabled and
        if (\count($enabledNotifications) > 0) {
            foreach ($enabledNotifications as $enabledNotification) {
                // get contactables
                $enabledAlert = $enabledNotification->alert;
                $alertRepository->setAndSendAlertMessage($enabledAlert);
            }
        }
    }
}
