<?php

namespace App\Jobs;

use App\Contracts\Repositories\AlertRepositoryInterface;
use App\Models\Alert;
use App\Models\AlertType;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AlertEmergencyListener implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     */
    public function handle(AlertRepositoryInterface $alertRepository)
    {
        // get emergency alert type ID
        $emergencyType = AlertType::where(['name' => config('global.alert.emergency.type')])->first();
        if (null === $emergencyType) {
            AdminNotifier::dispatch(
                'alert',
                trans('notifications.emergency.not-found'),
                'AlertType'
            )->onQueue('default');

            return;
        }
        // first, get emergency alerts immediatly
        $enabledAlerts = Alert::where([
            'is_draft'      => false,
            'is_enabled'    => true,
            'is_sent'       => false,
            'is_suspended'  => false,
            'alert_type_id' => $emergencyType->id
        ])->whereNotNull('programmed_at')->get();

        /*
         * Emergency alert notification policy
         * if is_sent is false and programmed_at is not null,  send the alert
         *
         */
        if (null !== $enabledAlerts) {
            // Get list if contact enabled
            foreach ($enabledAlerts as $enabledAlert) {
                $alertRepository->setAndSendAlertMessage($enabledAlert, true);
            }
        }
    }
}
