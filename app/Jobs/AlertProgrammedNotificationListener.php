<?php

namespace App\Jobs;

use App\Models\Alert;
use App\Models\AlertType;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AlertProgrammedNotificationListener implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        \Log::info('AlertProgrammedNotificationListener');
        $this->checkNotificationsToSend();
    }

    public function checkNotificationsToSend()
    {
        \Log::info('checkNotificationsToSend');

        // Run programmed alert first
        $typeHasInterval = config('global.alert.has_intervals');

        // Marge sur les temps de notification
        $delta = 60;
        $now = Carbon::now();

        $alertTypeWithInterval = AlertType::where(['name' => $typeHasInterval])->first();
        if (null === $alertTypeWithInterval) {
            AdminNotifier::dispatch(
                'alert',
                trans('notifications.interval.not-found'),
                'AlertType'
            )->onQueue('default');

            return;
        }
        // Check has interval firsts
        // Alerts With intervals
        $enabledAlertsWithInterval = Alert::where([
            'is_draft'      => false,
            'is_enabled'    => true,
            'is_sent'       => false,
            'is_suspended'  => false,
            'alert_type_id' => $alertTypeWithInterval->id
        ])->whereNotNull('programmed_at')->with(['alertNotificationTimes'])->get();

        \Log::info($enabledAlertsWithInterval);

        // verify notification interval or notification times
        if (\count($enabledAlertsWithInterval) > 0) {
            foreach ($enabledAlertsWithInterval as $enabledAlert) {
                if (null !== $enabledAlert->notification_interval) {
                    $diff = $enabledAlert->programmed_at->diffInSeconds($now);
                    if ($diff >= (int) $enabledAlert->notification_interval) {
                        $modulus = $diff % (int) $enabledAlert->notification_interval;
                        \Log::info('modulus : ' . $modulus);
                        if ($modulus <= $delta) {
                            // Send notifications
                            $notification = new Notification();
                            $notification->createEntry($enabledAlert, 'interval');
                            \Log::info('send Notifications :' . $enabledAlert->uuid);
                        }
                    }
                } elseif (\count($enabledAlert->alertNotificationTimes) > 0) {
                    foreach ($enabledAlert->alertNotificationTimes as $notificationTime) {
                        $diffFromNotif = $notificationTime->notify_at->diffInSeconds($now);
                        \Log::info('Diff : ' . $diffFromNotif);
                        if ($diffFromNotif <= $delta) {
                            // Send notifications
                            $notification = new Notification();
                            $notification->createEntry($enabledAlert, 'times');
                            \Log::info('send Notifications :' . $enabledAlert->uuid);
                        }
                    }
                }
            }
        }
    }
}
