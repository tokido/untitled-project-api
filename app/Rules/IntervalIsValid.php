<?php

namespace App\Rules;

use App\Models\AlertType;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class IntervalIsValid implements Rule
{
    protected $alertType;

    protected $is_datetime_string;

    protected $programmed_at;

    /**
     * Create a new rule instance.
     */
    public function __construct(AlertType $alertType, bool $is_datetime_string, $programmed_at)
    {
        $this->alertType = $alertType;
        $this->programmed_at = $programmed_at;
        $this->is_datetime_string = $is_datetime_string;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $now = Carbon::now();
        if (\in_array($this->alertType->name, config('global.alert.programmables'), true)) {
            $current = null;
            $programmed_at = null !== $this->programmed_at ? Carbon::createFromFormat(config('global.dates.api_input_format'), $this->programmed_at) : null;
            if (null !== $programmed_at) {
                $current = $value;
                if ($this->is_datetime_string) {
                    $current = Carbon::createFromFormat(config('global.dates.api_input_format'), $value);
                    if ($current instanceof Carbon) {
                        if ($current->greaterThan($programmed_at)) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    if ($now->addSeconds($current * 2) > $programmed_at) {
                        return false;
                    }
                }
            }
        }

        // if Black mail, return true
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The interval set is invalid, verify tour interval';
    }
}
