<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule as LaravelRule;

class MailOrNumber implements Rule
{
    protected $country;

    /**
     * Create a new rule instance.
     */
    public function __construct($country)
    {
        $this->country = $country;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $emailValidator = Validator::make([
            'identifier' => $value
        ], [
            'identifier' => 'email:rfc'
        ]);

        // @TODO update valid number list : https://laravel-phone.herokuapp.com/
        $numberValidator = Validator::make([
            'identifier' => $value,
            'country'    => $this->country
        ], [
            'identifier' => 'required|phone:country',
            'country'    => [
                'required_with:identifier',
                LaravelRule::in(config('global.i18n.countries'))
            ]
        ]);

        return (!$emailValidator->fails() || !$numberValidator->fails()) ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('rules.errors.mail-or-number');
    }
}
