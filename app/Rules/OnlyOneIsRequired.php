<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class OnlyOneIsRequired implements Rule
{
    protected $inputs;

    protected $comparison;

    /**
     * Create a new rule instance.
     */
    public function __construct(array $inputs, array $comparison)
    {
        $this->inputs = $inputs;
        $this->comparison = $comparison;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $diff = array_diff($this->comparison, $this->inputs);

        return (\count($diff) > 0) ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The field(s) ' . implode(', ', $this->comparison) . ' are paired and only use of one key is valid';
    }
}
