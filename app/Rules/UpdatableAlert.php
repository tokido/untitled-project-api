<?php

namespace App\Rules;

use App\Models\Alert;
use App\Models\AlertType;
use Illuminate\Contracts\Validation\Rule;

class UpdatableAlert implements Rule
{
    protected $alert;

    /**
     * Create a new rule instance.
     */
    public function __construct(Alert $alert)
    {
        $this->alert = $alert;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $creatablesType = AlertType::whereIn('name', config('global.alert.creatables'))
            ->pluck('uuid')
            ->toArray();

        /*
         * If updatable alert :  verify if updatable first & can change in another updatable
         * else must keep his old type if want to update
         */
        return ((\in_array($value, $creatablesType, true)
            && \in_array($this->alert->type->uuid, $creatablesType, true))
            || (!\in_array($value, $creatablesType, true)
            && !\in_array($this->alert->type->uuid, $creatablesType, true))
            && $this->alert->type->uuid === $value) ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('rules.errors.not-updatable-alert-type');
    }
}
