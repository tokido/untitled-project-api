<?php

namespace App\Rules;

use App\Models\Alert;
use Illuminate\Contracts\Validation\Rule;

class CreatableAlert implements Rule
{
    /**
     * Create a new rule instance.
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $creatablesType = \App\Models\AlertType::whereIn('name', config('global.alert.creatables'))
            ->pluck('uuid')
            ->toArray();

        // Get alert
        return (\in_array($value, $creatablesType, true)) ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('rules.errors.not-creatable-alert-type');
    }
}
