<?php

namespace App\Rules;

use App\Models\Contact;
use Illuminate\Contracts\Validation\Rule;

class IsNotLastEmergency implements Rule
{
    protected $contact;

    protected $user;

    /**
     * Create a new rule instance.
     */
    public function __construct(Contact $contact, $user)
    {
        $this->contact = $contact;
        $this->user = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (false === $value || 0 === (int) $value) {
            return Contact::whereNotIn('id', [$this->contact->id])
            ->where('is_emergency', true)
            ->where('user_id', $this->user->id)
            ->count() > 0 ? true : false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You must at least have on emergency contact.';
    }
}
