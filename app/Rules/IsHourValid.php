<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsHourValid implements Rule
{
    /**
     * Create a new rule instance.
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Why the fuck preg match return 1
        return 1 === preg_match('/^(2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]$/', $value) ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Hour format is invalid, it must be hh:mm:ss';
    }
}
