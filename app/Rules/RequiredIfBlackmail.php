<?php

namespace App\Rules;

use App\Models\AlertType;
use Illuminate\Contracts\Validation\Rule;

class RequiredIfBlackmail implements Rule
{
    protected $alertType;

    /**
     * Create a new rule instance.
     */
    public function __construct(AlertType $alertType)
    {
        $this->alertType = $alertType;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (\in_array($this->alertType->name, config('global.alert.untimables'), true)) ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This alert type doesn\'t support programmation date ';
    }
}
