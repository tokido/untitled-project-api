<?php

namespace App\Listeners;

use App\Events\AlertSent;

class GenerateContactAttachableRequest
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param AlertSent $event
     */
    public function handle(AlertSent $event)
    {
    }
}
