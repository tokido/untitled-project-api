<?php

namespace App\Models;

use App\Models\Traits\GetRouteKeyUuid;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class UploadedFile extends Model
{
    use GeneratesUuid;
    use GetRouteKeyUuid;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid'              => 'uuid',
        'decryption_start'  => 'datetime',
        'encryption_start'  => 'datetime',
        'encryption_end'    => 'datetime',
        'decryption_end'    => 'datetime'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'original_name',
        'generated_name',
        'path',
        'extension',
        'mime_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'generated_name',
        'path',
        'extension',
        'id',
        'deleted_at',
        'user_id',
        'uploaded_file_type_id',
        'updated_at'
    ];

    /**
     * The uploaded file type.
     */
    public function type()
    {
        return $this->belongsTo('App\Models\UploadedFileType', 'uploaded_file_type_id');
    }

    /**
     * Get the file owner.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Belongs to many folders.
     */
    public function folders()
    {
        return $this->belongsToMany('App\Models\Folder', 'folder_uploaded_file', 'uploaded_file_id', 'folder_id')->withPivot('created_at', 'updated_at', 'deleted_at');
    }

    /**
     * Get all alerts related to folder.
     */
    public function alerts()
    {
        return $this->morphToMany('App\Models\Alert', 'attachable')->withPivot('created_at', 'updated_at');
    }

    /**
     * Get uploaded files parts.
     */
    public function encryptedFiles()
    {
        return $this->hasMany('App\Models\EncryptedFile');
    }
}
