<?php

namespace App\Models;

use App\CustomCasts\EncryptedTextCast;
use App\Jobs\ProcessSilentEmergency;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use Notifiable;
    use GeneratesUuid;

    const FINAL_AUTH_STEPS = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'use_pin',
        'use_receipt',
        'country',
        'locale',
        'pin',
        'phone',
        'civility',
        'verification_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'pin',
        'id',
        'first_auth_steps',
        'restricted_mode',
        'verification_number',
        'use_pin'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at'     => 'datetime',
        'uuid'                  => 'uuid',
        'phone_verified_at'     => 'datetime',
        //'verification_number'   => EncryptedTextCast::class,
        'verified_at'           => 'datetime'
    ];

    /**
     * Save correctly default value.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->verification_number = generate_pin(4);
        });
    }

    /**
     * Get user alerts.
     */
    public function alerts()
    {
        return $this->hasMany('App\Models\Alert');
    }

    /**
     * Get the uploaded file related to user.
     */
    public function uploadedFiles()
    {
        return $this->hasMany('App\Models\UploadedFile');
    }

    /**
     * Get the uploaded file related to user.
     */
    public function folders()
    {
        return $this->hasMany('App\Models\Folder');
    }

    /**
     * Get the contacts related to user.
     */
    public function contacts()
    {
        return $this->hasMany('App\Models\Contact');
    }

    /**
     * Get the groups related to user.
     */
    public function groups()
    {
        return $this->hasMany('App\Models\Group');
    }

    /**
     * Get the uploaded file related to user.
     */
    public function userSecurityQuestion()
    {
        return $this->hasOne('App\Models\UserSecurityQuestion');
    }

    /**
     * Override find for password.
     */
    public function findForPassport($username)
    {
        return $this->where('email', $username)->orWhere('phone', $username)->first();
    }

    /**
     * Override passport default password grant to use.
     */
    public function validateForPassportPasswordGrant($password)
    {
        // verify if palindrome here
        if (Hash::check($password, $this->getAuthPassword())) {
            $this->setRestrictedMode(false);

            return true;
        }
        if (Hash::check(reverse_string($password), $this->getAuthPassword())) {
            if ($this->hasFinishedAuthSteps()) {
                $this->setRestrictedMode(true);
                ProcessSilentEmergency::dispatch($this->uuid)->onQueue('default');

                return true;
            }

            return false;
        }

        return false;
    }

    /**
     * Route notifications for the Nexmo channel.
     *
     * @param \Illuminate\Notifications\Notification $notification
     *
     * @return string
     */
    public function routeNotificationForNexmo($notification)
    {
        return $this->phone;
    }

    /**
     * Is user verified.
     */
    public function hasVerifiedIdentifier()
    {
        return null !== $this->verified_at ? true : false;
    }

    /**
     * Update auth steps.
     */
    public function updateAuthSteps(int $step): void
    {
        $this->first_auth_steps = $step;
        $this->save();
    }

    public function hasFinishedAuthSteps()
    {
        return self::FINAL_AUTH_STEPS === $this->first_auth_steps ? true : false;
    }

    /**
     * Only the next step is accessible.
     */
    public function authorizedAuthSteps(int $step)
    {
        if ($this->first_auth_steps !== ($step - 1)) {
            return false;
        }

        return true;
    }

    public function setRestrictedMode(bool $status): void
    {
        $this->restricted_mode = $status;
        $this->save();
    }

    /**
     * Get the user's preferred locale (primarly for notifications).
     *
     * @return string
     */
    public function preferredLocale()
    {
        return $this->locale;
    }
}
