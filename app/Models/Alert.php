<?php

namespace App\Models;

use App\Models\Traits\GetRouteKeyUuid;
use Carbon\Carbon;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alert extends Model
{
    use GeneratesUuid;
    use GetRouteKeyUuid;
    use SoftDeletes;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'sent_at'           => 'datetime',
        'programmed_at'     => 'datetime',
        'uuid'              => 'uuid'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'message',
        'notification_interval',
        'location_coordinates',
        'is_draft',
        'is_enabled',
        'programmed_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'deleted_at',
        'user_id',
        'alert_type_id'
    ];

    /**
     * Get the alert owner.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the alert type.
     */
    public function type()
    {
        return $this->belongsTo('App\Models\AlertType', 'alert_type_id');
    }

    /**
     * Get the contact.
     */
    public function contacts()
    {
        return $this->morphedByMany('App\Models\Contact', 'alertable')->withPivot('created_at', 'updated_at');
    }

    /**
     * Contact attachableRequest.
     */
    public function contactAttachableRequests()
    {
        return $this->hasMany('App\Models\ContactAttachableRequest');
    }

    /**
     * Get the groups related to the alert.
     */
    public function groups()
    {
        return $this->morphedByMany('App\Models\Group', 'alertable')->withPivot('created_at', 'updated_at');
    }

    /**
     * Get the contact.
     */
    public function folders()
    {
        return $this->morphedByMany('App\Models\Folder', 'attachable')->withPivot('created_at', 'updated_at');
    }

    /**
     * Get the groups related to the alert.
     */
    public function uploadedFiles()
    {
        return $this->morphedByMany('App\Models\UploadedFile', 'attachable')->withPivot('created_at', 'updated_at');
    }

    /**
     * Has one or many notification time.
     */
    public function alertNotificationTimes()
    {
        return $this->hasMany('App\Models\AlertNotificationTime');
    }

    /**
     * Has one or many notification recurrence.
     */
    public function alertNotificationRecurrences()
    {
        return $this->hasMany('App\Models\AlertNotificationRecurrence');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\Notification');
    }

    public function setAsSent($isEmergency = false)
    {
        $this->is_sent = true;
        $this->is_enabled = true;
        $this->is_draft = false;
        $this->is_suspended = false;
        $this->sent_at = Carbon::now();
        if (true === $isEmergency) {
            // Delete programmed_at and set is sent as false so user can re-send
            $this->programmed_at = null;
            $this->is_sent = false;
        }
        $this->notifications()->update([
            'is_waiting_response' => false,
        ]);

        $this->save();
    }

    public function setAsDisabled()
    {
        $this->is_sent = false;
        $this->is_enabled = false;
        $this->is_draft = false;
        $this->is_suspended = false;
        $this->notifications()->update([
            'is_waiting_response' => false,
        ]);

        $this->save();
    }

    public function setAsSuspended()
    {
        $this->is_sent = false;
        $this->is_enabled = true;
        $this->is_draft = false;
        $this->is_suspended = true;
        $this->notifications()->update([
            'is_waiting_response' => false,
        ]);

        $this->save();
    }

    public function setAsDraft()
    {
        $this->is_sent = false;
        $this->is_enabled = false;
        $this->is_draft = true;
        $this->is_suspended = false;
        $this->notifications()->update([
            'is_waiting_response' => false,
        ]);

        $this->save();
    }

    public function setAsEnabled()
    {
        $this->is_sent = false;
        $this->is_enabled = true;
        $this->is_draft = false;
        $this->is_suspended = false;
        $this->notifications()->update([
            'is_waiting_response' => true,
        ]);

        $this->save();
    }

    public function getZipPath()
    {
        return config('global.files.paths.temp') . $this->uuid . '.zip';
    }

    public function isDeletable()
    {
        // validatoin from database  : better check false because tinyInt Conversion to bool is BULLSHIT
        return false === $this->type->is_deletable ? false : true;
    }
}
