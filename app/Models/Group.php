<?php

namespace App\Models;

use App\Models\Traits\GetRouteKeyUuid;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use GeneratesUuid;
    use GetRouteKeyUuid;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid'          => 'uuid',
        'is_emergency'  => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'is_emergency'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'updated_at',
        'user_id'
    ];

    /**
     * The group owner.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * The users that belong to the role
     * Casts on fetch.
     */
    public function contacts()
    {
        return $this->belongsToMany('App\Models\Contact', 'group_contact', 'group_id', 'contact_id');
    }

    /**
     * Get all alerts related to group
     * Casts on fetch.
     */
    public function alerts()
    {
        return $this->morphToMany('App\Models\Alert', 'alertable')->withPivot('created_at', 'updated_at');
    }
}
