<?php

namespace App\Models;

use App\Models\Traits\GetRouteKeyUuid;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class SecurityQuestion extends Model
{
    use GeneratesUuid;
    use GetRouteKeyUuid;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid'          => 'uuid',
        'is_enabled'    => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label',
        'is_enabled'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'updated_at',
        'is_enabled'
    ];

    /**
     * Get the answered security questions.
     */
    public function userSecurityQuestions()
    {
        return $this->hasMany('App\Models\UserSecurityQuestion');
    }
}
