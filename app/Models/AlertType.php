<?php

namespace App\Models;

use App\Models\Traits\GetRouteKeyUuid;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class AlertType extends Model
{
    use GeneratesUuid;
    use GetRouteKeyUuid;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid'          => 'uuid',
        'is_creatable'  => 'boolean',
        'is_deletable'  => 'boolean',
    ];

    /**
     * Mass assignables parameters.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'is_creatable',
        'is_deletable'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'created_at',
        'updated_at'
    ];

    /**
     * Get the alerts related from type.
     */
    public function alerts()
    {
        return $this->hasMany('App\Models\Alert', 'alert_type_id');
    }

    /**
     * get the model by name instead of id.
     *
     * @var string
     */
    public function getRouteKeyName(): string
    {
        return 'name';
    }
}
