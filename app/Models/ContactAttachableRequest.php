<?php

namespace App\Models;

use App\Models\Traits\GetRouteKeyUuid;
use Carbon\Carbon;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class ContactAttachableRequest extends Model
{
    use GeneratesUuid;
    use GetRouteKeyUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'requested_at',
        'downloaded_at',
        'block',
        'size'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'expires_at'        => 'datetime',
        'requested_at'      => 'datetime',
        'ready_at'          => 'datetime',
        'downloaded_at'     => 'datetime',
        'uuid'              => 'uuid',
        'is_download_ready' => 'boolean'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'updated_at',
        'random_password',
        'random_id',
        'request_count',
        'expires_at'
    ];

    public function alert()
    {
        return $this->belongsTo('App\Models\Alert');
    }

    public function contact()
    {
        return $this->belongsTo('App\Models\Contact');
    }

    public function saveContactRequest()
    {
        $this->request_count = (int) $this->request_count + 1;
        $this->requested_at = Carbon::now();
        $this->save();
    }

    public function setContactRequestDowloadReady()
    {
        $now = Carbon::now();
        $this->is_download_ready = true;
        $this->ready_at = $now;
        $this->expires_at = $now->addHours(config('global.files.download_expiration'));

        $this->save();
    }

    public function setContactRequestDownload()
    {
        $this->download_count = (int) $this->donwload_count + 1;
        $this->downloaded_at = Carbon::now();

        $this->save();
    }

    public function restorePristineStatus()
    {
        $this->is_download_ready = false;
        $this->ready_at = null;
        $this->expires_at = null;

        $this->save();
    }
}
