<?php

namespace App\Models;

use App\Models\Traits\GetRouteKeyUuid;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Folder extends Model
{
    use GeneratesUuid;
    use GetRouteKeyUuid;
    use SoftDeletes;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid'  => 'uuid'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'updated_at',
        'user_id',
        'deleted_at'
    ];

    /**
     * Get the folder owner.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * has many files.
     */
    public function uploadedFiles()
    {
        return $this->belongsToMany('App\Models\UploadedFile', 'folder_uploaded_file', 'folder_id', 'uploaded_file_id')->withPivot('created_at', 'updated_at', 'deleted_at');
    }

    /**
     * Get all alerts related to folder.
     */
    public function alerts()
    {
        return $this->morphToMany('App\Models\Alert', 'attachable')->withPivot('created_at', 'updated_at');
    }
}
