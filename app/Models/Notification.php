<?php

namespace App\Models;

use App\Models\Traits\GetRouteKeyUuid;
use Carbon\Carbon;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use GeneratesUuid;
    use GetRouteKeyUuid;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid'                  => 'uuid',
        'sent_at'               => 'datetime',
        'disabled_at'           => 'datetime',
        'expires_at'            => 'datetime',
        'is_waiting_response'   => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'notification_from',
        'disabled_from',
        'disabled_at',
        'ocurrence_left'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'is_waiting_response',
        'updated_at',
        'user_id',
        'alert_id'
    ];

    public function alert()
    {
        return $this->belongsTo('App\Models\Alert');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function createEntry($alert, $from, $sent_to = 'both')
    {
        $now = Carbon::now();
        $this->is_waiting_response = true;
        $this->notification_from = $from;
        $this->sent_to = $sent_to;
        $this->sent_at = $now;
        $this->expires_at = $now->addMinutes(10); // Expiration d'une alerte peut être configurable dans l'admin user
        $this->ocurrence_left = config('global.alert.notifications.default_ocurrence');
        $this->alert()->associate($alert);
        $this->user()->associate($alert->user);

        $this->save();
    }

    public function disable($disabled_from = null)
    {
        $this->disabled_from = null !== $disabled_from ? $disabled_from : 'other';
        $this->disabled_at = Carbon::now();
        $this->is_waiting_response = false;
        $this->was_disabled = true;

        $this->save();
    }
}
