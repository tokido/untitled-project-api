<?php

namespace App\Models;

use App\Models\Traits\GetRouteKeyUuid;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class UserSecurityQuestion extends Model
{
    use GeneratesUuid;
    use GetRouteKeyUuid;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid'  => 'uuid'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'answer',
        'tip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'user_id',
        'security_question_id',
        'created_at'
    ];

    /**
     * The question owner.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * The Selected Questions.
     */
    public function securityQuestion()
    {
        return $this->belongsTo('App\Models\SecurityQuestion');
    }
}
