<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AlertNotificationTime extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'notify_at'           => 'datetime',
        'is_recurrent'        => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'notify_at',
        'is_recurrent'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'updated_at',
        'alert_id',
        'deleted_at'
    ];

    /**
     * Get the alert owner.
     */
    public function alert()
    {
        return $this->belongsTo('App\Models\Alert');
    }
}
