<?php

namespace App\Models;

use App\Models\Traits\GetRouteKeyUuid;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Contact extends Model
{
    use GeneratesUuid;
    use GetRouteKeyUuid;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'email',
        'country',
        'is_emergency'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'updated_at',
        'user_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid'          => 'uuid',
        'is_emergency'  => 'boolean'
    ];

    /**
     * Get the alert owner.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * groups where belongs the user.
     */
    public function groups()
    {
        return $this->belongsToMany('App\Models\Group', 'group_contact', 'contact_id', 'group_id');
    }

    /**
     * Get all alerts related to contact.
     */
    public function alerts()
    {
        return $this->morphToMany('App\Models\Alert', 'alertable')->withPivot('created_at', 'updated_at');
    }

    public function routeNotificationForNexmo($notification)
    {
        return $this->phone;
    }

    public function contactAttachableRequests()
    {
        return $this->hasMany('App\Models\ContactAttachableRequest');
    }
}
