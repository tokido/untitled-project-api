<?php

namespace App\Models;

use App\Models\Traits\GetRouteKeyUuid;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class UploadedFileType extends Model
{
    use GeneratesUuid;
    use GetRouteKeyUuid;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid'           => 'uuid',
        'is_default'     => 'boolean',
        'is_enabled'     => 'boolean',
        'is_encryptable' => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'is_default',
        'is_enabled',
        'is_encryptable'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'is_default',
        'is_enabled',
        'is_encryptable',
        'updated_at'
    ];

    /**
     * Get the uploaded file related to type.
     */
    public function uploadedFiles()
    {
        return $this->hasMany('App\Models\UploadedFile');
    }
}
