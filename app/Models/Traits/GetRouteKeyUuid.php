<?php

namespace App\Models\Traits;

trait GetRouteKeyUuid
{
    /**
     * get the model by uuid instead of id.
     *
     * @var string
     */
    public function getRouteKeyName(): string
    {
        return 'uuid';
    }
}
