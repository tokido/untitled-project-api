<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Route;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $exception
     *
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $exception)
    {
        $error = $this->convertExceptionToResponse($exception);

        $response = [
            'success' => false,
            'message' => $exception->getMessage(),
            'data'    => $exception->getCode()
        ];

        if (!\in_array(config('app.env'), ['production', 'prod', 'preprod'], true)) {
            $response['trace'] = $exception->getTraceAsString();
        } else {
            //@TODO : catch all exception errors for Production & preprod
            return Route::respondWithRoute('api.fallback.404');
        }

        return response()->json($response, $error->getStatusCode());
    }
}
