<?php

namespace App\Observers;

use App\Models\Alert;
use App\Models\AlertType;
use App\Models\Group;

class GroupObserver
{
    /**
     * Handle the Group "created" event.
     *
     * @param \App\Models\Group $group
     */
    public function created(Group $group)
    {
        if (true === $group->is_emergency) {
            $this->setEmergencyAlertable($group);
        }
    }

    /**
     * Handle the Group "updated" event.
     *
     * @param \App\Models\Group $group
     */
    public function updated(Group $group)
    {
        if (true === $group->is_emergency) {
            $this->setEmergencyAlertable($group);
        } else {
            $this->unsetEmergencyAlertable($group);
        }
    }

    /**
     * Handle the Group "deleted" event.
     *
     * @param \App\Models\Group $group
     */
    public function deleted(Group $group)
    {
        $this->unsetEmergencyAlertable($group);
    }

    /**
     * Handle the Group "restored" event.
     *
     * @param \App\Models\Group $group
     */
    public function restored(Group $group)
    {
        if (true === $group->is_emergency) {
            $this->setEmergencyAlertable($group);
        }
    }

    /**
     * Handle the Group "force deleted" event.
     *
     * @param \App\Models\Group $group
     */
    public function forceDeleted(Group $group)
    {
        $this->unsetEmergencyAlertable($group);
    }

    public function setEmergencyAlertable(Group $group)
    {
        $emergencyType = AlertType::where(['name' => config('global.alert.emergency.type')])->first();
        // Get Emergency Alert
        $emergencies = Alert::where('alert_type_id', $emergencyType->id)->get();
        foreach ($emergencies as $emergencyAlert) {
            $emergencyAlert->groups()->attach($group);
        }
    }

    public function unsetEmergencyAlertable(Group $group)
    {
        $emergencyType = AlertType::where(['name' => config('global.alert.emergency.type')])->first();
        // Get Emergency Alert
        $emergencies = Alert::where('alert_type_id', $emergencyType->id)->get();
        foreach ($emergencies as $emergencyAlert) {
            $emergencyAlert->groups()->detach($group);
        }
    }
}
