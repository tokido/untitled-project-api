<?php

namespace App\Observers;

use App\Contracts\Repositories\AlertRepositoryInterface;
use App\Models\User;
use App\Notifications\VerifyIdentifier;

class UserObserver
{
    protected $alertRepository;

    public function __construct(
        AlertRepositoryInterface $alertRepository
    ) {
        $this->alertRepository = $alertRepository;
    }

    /**
     * Listen to the User created event.
     *
     * @param User $user
     */
    public function created(User $user)
    {
        // Send validation
        $user->notify(new VerifyIdentifier($user));

        // Create Emergency-Alert
        $this->alertRepository->createDefaultEmergencyAlert($user);
        // Create Default intrusion alert
        $this->alertRepository->createDefaultIntrusionAlert($user);
    }
}
