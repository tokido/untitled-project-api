<?php

namespace App\Observers;

use App\Models\Alert;
use App\Models\AlertType;
use App\Models\Contact;

class ContactObserver
{
    /**
     * Handle the contact "created" event.
     *
     * @param \App\Models\Contact $contact
     */
    public function created(Contact $contact)
    {
        if (true === $contact->is_emergency) {
            $this->setEmergencyAlertable($contact);
        }
    }

    /**
     * Handle the contact "updated" event.
     *
     * @param \App\Models\Contact $contact
     */
    public function updated(Contact $contact)
    {
        if (true === $contact->is_emergency) {
            $this->setEmergencyAlertable($contact);
        } else {
            $this->unsetEmergencyAlertable($contact);
        }
    }

    /**
     * Handle the contact "deleted" event.
     *
     * @param \App\Models\Contact $contact
     */
    public function deleted(Contact $contact)
    {
        $this->unsetEmergencyAlertable($contact);
    }

    /**
     * Handle the contact "restored" event.
     *
     * @param \App\Models\Contact $contact
     */
    public function restored(Contact $contact)
    {
        if (true === $contact->is_emergency) {
            $this->setEmergencyAlertable($contact);
        } else {
            $this->unsetEmergencyAlertable($contact);
        }
    }

    /**
     * Handle the contact "force deleted" event.
     *
     * @param \App\Models\Contact $contact
     */
    public function forceDeleted(Contact $contact)
    {
        $this->unsetEmergencyAlertable($contact);
    }

    public function setEmergencyAlertable(Contact $contact)
    {
        $emergencyType = AlertType::where(['name' => config('global.alert.emergency.type')])->first();
        // Get Emergency Alert
        $emergencies = Alert::where('alert_type_id', $emergencyType->id)->get();
        foreach ($emergencies as $emergencyAlert) {
            $emergencyAlert->contacts()->attach($contact);
        }
    }

    public function unsetEmergencyAlertable(Contact $contact)
    {
        $emergencyType = AlertType::where(['name' => config('global.alert.emergency.type')])->first();
        // Get Emergency Alert
        $emergencies = Alert::where('alert_type_id', $emergencyType->id)->get();
        foreach ($emergencies as $emergencyAlert) {
            $emergencyAlert->contacts()->detach($contact);
        }
    }
}
