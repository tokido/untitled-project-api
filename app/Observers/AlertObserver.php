<?php

namespace App\Observers;

use App\Models\Alert;

class AlertObserver
{
    public function __construct()
    {
    }

    /**
     * Listen to the Alert deleting event.
     *
     * @param Alert $alert
     */
    public function deleting(Alert $alert)
    {
        // Delete all morphables
        $alert->groups()->delete();
        $alert->contacts()->delete();
        $alert->folders()->delete();
        $alert->uploadedFiles()->delete();
    }
}
