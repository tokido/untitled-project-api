<?php

namespace App\Contracts\Repositories;

use App\Models\Contact;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface ContactRepositoryInterface extends RepositoryInterface
{
    /**
     * Create an entity.
     *
     * @param User       $user
     * @param Collection $contacts
     *
     * @return bool
     */
    public function areContactsOwnedByUser(User $user, Collection $contacts): bool;

    /**
     * check if contact in groups.
     *
     * @param Contact $contact
     * @param $groups
     *
     * @return bool
     */
    public function areContactInGroups(Contact $contact, $groups): bool;
}
