<?php

namespace App\Contracts\Repositories;

use App\Models\Alert;
use App\Models\User;

interface AlertRepositoryInterface extends RepositoryInterface
{
    /**
     * Create default emergency alert for new user.
     *
     * @param User $user
     *
     * @return Alert
     */
    public function createDefaultEmergencyAlert(User $user): Alert;

    /**
     * Create default intrusion alert for new user.
     *
     * @param User $user
     *
     * @return Alert
     */
    public function createDefaultIntrusionAlert(User $user): Alert;
}
