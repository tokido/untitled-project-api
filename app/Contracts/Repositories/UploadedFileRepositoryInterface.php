<?php

namespace App\Contracts\Repositories;

use App\Models\User;

interface UploadedFileRepositoryInterface extends RepositoryInterface
{
    /**
     * check if files exists and are owned by user.
     *
     * @param User $files
     * @param $groups
     *
     * @return bool
     */
    public function areUploadedFilesOwnedByUser(User $user, $uploadedFiles): bool;
}
