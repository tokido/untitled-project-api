<?php

namespace App\Contracts\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    /**
     * Make a new instance of the entity to query on.
     *
     * @param array|string $with
     *
     * @return Model|Builder
     */
    public function make(array $with = []);

    /**
     * Get model by key.
     *
     * @param string $key
     * @param $value
     *
     * @return Model
     */
    public function findBy(string $key, $value): ?Model;

    /**
     * Get model by Uuid.
     *
     * @param string $uuid
     *
     * @return Model
     */
    public function findByUuid(string $uuid): ?Model;

    /**
     * Create an entity.
     *
     * @param string $uuid
     *
     * @return Model
     */
    public function create(array $input): ?Model;

    /**
     * Get all.
     *
     * @param string $uuid
     *
     * @return Collection
     */
    public function all(array $where = [], array $with = []);

    /**
     * create new instance of the model, particularly useful for Hydratations.
     *
     * @return Model
     */
    public function newInstance(): ?Model;
}
