<?php

namespace App\Contracts\Repositories;

use App\Models\Group;
use App\Models\User;

interface GroupRepositoryInterface extends RepositoryInterface
{
    /**
     * Create an entity.
     *
     * @param User $user
     * @param $groups
     *
     * @return bool
     */
    public function areGroupsOwnedByUser(User $user, $groups): bool;

    /**
     * Create an entity.
     *
     * @param Group $group
     * @param $contacts
     *
     * @return bool
     */
    public function areContactsInGroup(Group $group, $contacts): bool;
}
