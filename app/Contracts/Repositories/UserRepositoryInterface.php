<?php

namespace App\Contracts\Repositories;

use App\Models\User;

interface UserRepositoryInterface extends RepositoryInterface
{
    /**
     * Verify user by identifier.
     *
     * @param string $identifier
     *
     * @return bool
     */
    public function isUserIdentifierExist(string $identifier): bool;

    /**
     * Get user by username or Phome.
     *
     * @param string $identifier
     *
     * @return User
     */
    public function getUserByIdentifier(string $identifier): ?User;

    /**
     * Check if user already verified (verified_at).
     *
     * @param User $user
     *
     * @return bool
     */
    public function isVerified(User $user): bool;

    /**
     * Check if user already verified (verified_at).
     *
     * @param User $user
     * @param int  $verification_number
     *
     * @return bool
     */
    public function verifyAccount(User $user, int $verification_number): bool;

    /**
     * Create new user.
     *
     * @param array $input
     *
     * @return User|null
     */
    public function registerUser(array $input): ?User;

    /**
     * add notifiableFieldOnUpdate : update field if notifiable then notify the user to confirm.
     *
     * @param User   $user
     * @param string $field
     * @param string $value
     *
     * @return bool
     */
    public function addNotifiableFieldOnUpdate(User $user, string $field, string $value): bool;
}
