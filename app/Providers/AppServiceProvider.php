<?php

namespace App\Providers;

use App\Models\Alert;
use App\Models\Contact;
use App\Models\Group;
use App\Models\User;
use App\Observers\AlertObserver;
use App\Observers\ContactObserver;
use App\Observers\GroupObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
        // Base repository
        $this->app->bind(
            'App\Contracts\Repositories\RepositoryInterface',
            'App\Repositories\Repository'
        );

        // Contact Repository
        $this->app->bind(
            'App\Contracts\Repositories\ContactRepositoryInterface',
            'App\Repositories\ContactRepository'
        );

        // User Repository
        $this->app->bind(
            'App\Contracts\Repositories\UserRepositoryInterface',
            'App\Repositories\UserRepository'
        );

        // Group Repository
        $this->app->bind(
            'App\Contracts\Repositories\GroupRepositoryInterface',
            'App\Repositories\GroupRepository'
        );

        // Alert Repository
        $this->app->bind(
            'App\Contracts\Repositories\AlertRepositoryInterface',
            'App\Repositories\AlertRepository'
        );

        // SecurityQestion Repository
        $this->app->bind(
            'App\Contracts\Repositories\SecurityQuestionRepositoryInterface',
            'App\Repositories\SecurityQuestionRepository'
        );
        $this->app->bind(
            'App\Contracts\Repositories\UserAnswerRepositoryInterface',
            'App\Repositories\UserAnswerRepository'
        );

        // Alert Notification time
        $this->app->bind(
            'App\Contracts\Repositories\AlertNotificationTimeRepositoryInterface',
            'App\Repositories\AlertNotificationTimeRepository'
        );
        $this->app->bind(
            'App\Contracts\Repositories\AlertTypeRepositoryInterface',
            'App\Repositories\AlertTypeRepository'
        );

        //Files & Folder Repositories
        $this->app->bind(
            'App\Contracts\Repositories\FolderRepositoryInterface',
            'App\Repositories\FolderRepository'
        );
        $this->app->bind(
            'App\Contracts\Repositories\UploadedFileRepositoryInterface',
            'App\Repositories\UploadedFileRepository'
        );

        //Translations
        $this->app->bind(
            'App\Contracts\Repositories\TranslationRepositoryInterface',
            'App\Repositories\TranslationRepository'
        );

        $this->app->bind(
            'App\Contracts\Repositories\ContactAttachableRequestRepositoryInterface',
            'App\Repositories\ContactAttachableRequestRepository'
        );

        $this->app->bind(
            'App\Contracts\Repositories\NotificationRepositoryInterface',
            'App\Repositories\NotificationRepository'
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        // UF8MB4 Maxx length on old versions
        //\Schema::defaultStringLength(191);
        User::observe(UserObserver::class);
        Alert::observe(AlertObserver::class);
        Contact::observe(ContactObserver::class);
        Group::observe(GroupObserver::class);
    }
}
