<?php

namespace App\Providers;

use App\Facades\Services\ApiDate;
use App\Facades\Services\FileEncryption;
use Illuminate\Support\ServiceProvider;

class FacadeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
        $this->app->bind('apiDate', function () {
            return new ApiDate();
        });

        $this->app->bind('fileEncryption', function () {
            return new FileEncryption();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
    }
}
