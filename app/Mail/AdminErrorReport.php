<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminErrorReport extends Mailable
{
    use Queueable;
    use SerializesModels;

    protected $process;

    protected $reportedError;

    protected $reportedClassName;

    protected $uuid;

    /**
     * Create a new message instance.
     */
    public function __construct(
        string $process,
        string $reportedError,
        string $reportedClassName = null,
        string $uuid = null
    ) {
        $this->reportedError = $reportedError;
        $this->process = $process;
        $this->reportedClassName = $reportedClassName;
        $this->uuid = $uuid;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.admin-report')
            ->with([
                'admin'                 => config('global.admin.email'),
                'now'                   => Carbon::now(),
                'process'               => $this->process,
                'reportedError'         => $this->reportedError,
                'reportedClassName'     => $this->reportedClassName,
                'uuid'                  => $this->uuid
            ]);
    }
}
