<?php

namespace App\Console\Commands;

use App\Jobs\AlertEmergencyListener;
use Illuminate\Console\Command;

class ListenAlertEmergencyNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert:emergency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verify if an emergency alert has to be sent';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        AlertEmergencyListener::dispatch()->onQueue('default');
    }
}
