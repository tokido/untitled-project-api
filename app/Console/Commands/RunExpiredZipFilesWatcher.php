<?php

namespace App\Console\Commands;

use App\Jobs\WatchExpiredZipFiles;
use Illuminate\Console\Command;

class RunExpiredZipFilesWatcher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'compressedfiles:watch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete expired zip files';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        WatchExpiredZipFiles::dispatch()->onQueue('default');
    }
}
