<?php

namespace App\Console\Commands;

use App\Jobs\AlertBlackmailNotificationListener;
use App\Jobs\AlertProgrammedNotificationListener;
use Illuminate\Console\Command;

class ListenAlertNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert:listen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verify if a programmed alert';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        AlertProgrammedNotificationListener::dispatch()->onQueue('default');
        AlertBlackmailNotificationListener::dispatch()->onQueue('default');
    }
}
