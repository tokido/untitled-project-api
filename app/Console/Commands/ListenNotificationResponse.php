<?php

namespace App\Console\Commands;

use App\Jobs\NotificationResponseActions;
use Illuminate\Console\Command;

class ListenNotificationResponse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert:notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen to active alert if notifications were disabled';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        NotificationResponseActions::dispatch()->onQueue('default');
    }
}
