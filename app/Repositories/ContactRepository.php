<?php

namespace App\Repositories;

use App\Contracts\Repositories\ContactRepositoryInterface;
use App\Models\Contact;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class ContactRepository extends Repository implements ContactRepositoryInterface
{
    public function __construct(
        Contact $model
    ) {
        $this->model = $model;
    }

    public function areContactsOwnedByUser(User $user, Collection $contacts): bool
    {
        foreach ($contacts as $contact) {
            if (!$user->contacts->contains($contact)) {
                return false;
            }
        }

        return true;
    }

    public function areContactInGroups(Contact $contact, $groups): bool
    {
        foreach ($groups as $group) {
            if (!$contact->contacts->contains($group)) {
                return false;
            }
        }

        return true;
    }

    public function countAlertEmergencyLeft(array $deletableIds, User $user)
    {
        return $this->model
            ->whereNotIn('id', $deletableIds)
            ->where('is_emergency', true)
            ->where('user_id', $user->id)
            ->count() > 0 ? true : false;
    }
}
