<?php

namespace App\Repositories;

use App\Contracts\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

abstract class Repository implements RepositoryInterface
{
    /**
     * @var Model|null
     */
    protected $model = null;

    /**
     * Make a new instance of the entity to query on.
     *
     * @param array|string $with
     *
     * @return Model|Builder
     */
    public function make(array $with = [])
    {
        return $this->model->with($with);
    }

    public function findBy(string $key, $value): ?Model
    {
        return $this->model->where($key, $value)->first();
    }

    public function findByUuid(string $uuid): ?Model
    {
        return $this->model->whereUuid($uuid)->first();
    }

    public function findManyByUuid(array $uuids)
    {
        return $this->model->whereUuid($uuids)->get();
    }

    public function create(array $input): ?Model
    {
        return $this->model->create($input);
    }

    public function newInstance(): ?Model
    {
        $current = \get_class($this->model);

        return new $current();
    }

    public function all(array $where = [], array $with = [])
    {
        return $this->model->where($where)->with($with)->get();
    }
}
