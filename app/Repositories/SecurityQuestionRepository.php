<?php

namespace App\Repositories;

use App\Contracts\Repositories\SecurityQuestionRepositoryInterface;
use App\Models\SecurityQuestion;

class SecurityQuestionRepository extends Repository implements SecurityQuestionRepositoryInterface
{
    public function __construct(
        SecurityQuestion $model
    ) {
        $this->model = $model;
    }
}
