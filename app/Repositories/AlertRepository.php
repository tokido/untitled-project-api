<?php

namespace App\Repositories;

use App\Contracts\Repositories\AlertRepositoryInterface;
use App\Models\Alert;
use App\Models\Alertable;
use App\Models\AlertNotificationTime;
use App\Models\AlertType;
use App\Models\Contact;
use App\Models\ContactAttachableRequest;
use App\Models\Folder;
use App\Models\Group;
use App\Models\UploadedFile;
use App\Models\User;
use App\Notifications\AlertNotification;

class AlertRepository extends Repository implements AlertRepositoryInterface
{
    /**
     * Attributes.
     */
    protected $alertTypeModel;

    protected $alertNotificationTime;

    protected $folderModel;

    protected $uploadedFileModel;

    protected $groupModel;

    protected $contactModel;

    protected $contactAttachableRequestModel;

    protected $alertableModel;

    // Model map with authorized String
    const MODEL_MAP = [
        'folder'    => [
            'model'         => 'folderModel',
            'association'   => 'folders'
        ],
        'contact'   => [
            'model'         => 'contactModel',
            'association'   => 'contacts'
        ],
        'group'     => [
            'model'         => 'groupModel',
            'association'   => 'groups'
        ],
        'file'      => [
            'model'         => 'uploadedFileModel',
            'association'   => 'uploadedFiles'
        ]
    ];

    public function __construct(
        Alert $model,
        AlertType $alertTypeModel,
        AlertNotificationTime $alertNotificationTime,
        Folder $folderModel,
        UploadedFile $uploadedFileModel,
        Contact $contactModel,
        Group $groupModel,
        ContactAttachableRequest $contactAttachableRequestModel,
        Alertable $alertableModel
    ) {
        $this->model = $model;
        $this->alertTypeModel = $alertTypeModel;
        $this->alertNotificationTime = $alertNotificationTime;
        $this->folderModel = $folderModel;
        $this->uploadedFileModel = $uploadedFileModel;
        $this->contactModel = $contactModel;
        $this->groupModel = $groupModel;
        $this->contactAttachableRequestModel = $contactAttachableRequestModel;
        $this->alertableModel = $alertableModel;
    }

    public function getUserAlertsByType(User $user, $typeName = null)
    {
        if (null === $typeName) {
            $typeName = config('global.alert.emergency.type');
        }
        $type = $this->alertTypeModel->where(['name' => $typeName])->first();

        return $this->model->where([
            'alert_type_id' => $type->id,
            'user_id'       => $user->id
        ])->get();
    }

    protected function createDefaultAlert(User $user, array $type): Alert
    {
        // get alert type emergency
        $emergencyAlertType = $this->alertTypeModel->where('name', $type['type'])->first();
        $emergency = $this->newInstance();
        $emergency = $emergency->fill([
            'title'                 => trans($type['title']),
            'message'               => trans($type['message']),
            'is_draft'              => false,
            'is_enabled'            => true,
            'is_suspended'          => false,
        ]);

        $emergency->type()->associate($emergencyAlertType);
        $emergency->user()->associate($user);
        $emergency->save();

        return $emergency;
    }

    public function createDefaultIntrusionAlert(User $user): Alert
    {
        $type = config('global.alert.intrusion');

        return $this->createDefaultAlert($user, $type);
    }

    public function createDefaultEmergencyAlert(User $user): Alert
    {
        $type = config('global.alert.emergency');

        return $this->createDefaultAlert($user, $type);
    }

    public function verifyAndGetMorphables(array $morphables, User $user): ?array
    {
        $morphableEntities = [];

        if (0 === \count($morphables)) {
            return null;
        }

        foreach ($morphables as $morphable) {
            if (!\array_key_exists(mb_strtolower($morphable['type']), self::MODEL_MAP)) {
                return null;
            }

            $model = self::MODEL_MAP[mb_strtolower($morphable['type'])]['model'];
            $associations = self::MODEL_MAP[mb_strtolower($morphable['type'])]['association'];
            $entity = $this->$model->whereUuid($morphable['uuid'])->first();
            if (null === $entity || !$user->$associations->contains($entity)) {
                return null;
            }
            array_push($morphableEntities, $entity);
        }

        return $morphableEntities;
    }

    public function requestDownloadAttachables(Alert $alert)
    {
        $downloadableFolders = collect([]);
        foreach ($alert->folders as $folder) {
            $downloadableFolders->put($folder->name . '-' . $folder->uuid, $folder->uploadedFiles);
        }

        return collect([
            'files'     => $alert->uploadedFiles,
            'folders'   => $downloadableFolders
        ]);
    }

    public function setAndSendAlertMessage(Alert $enabledAlert, $is_emergency = false)
    {
        $hasAttachableRequest = false;
        // Get alertables
        $alertContacts = collect([]);
        $alertContacts = $enabledAlert->contacts;
        $groups = $enabledAlert->groups;
        // @TODO refactoring remove foreach ad to merge
        if (\count($groups) > 0) {
            foreach ($groups as $group) {
                foreach ($group->contacts as $contact) {
                    $alertContacts->push($contact);
                }
            }
        }
        // Check if alert has attachable and create entry on notification :
        if (\count($enabledAlert->folders()->get()) > 0 || \count($enabledAlert->uploadedFiles()->get()) > 0) {
            $hasAttachableRequest = true;
        }
        // If no contact attached to the alert, send it to default contact
        // Emergency contact are mapped to emergency
        if (0 === \count($alertContacts)) {
            $alertContacts = $enabledAlert
                ->user
                ->contacts()
                ->where('is_emergency', true)
                ->get();
        }
        // Create email notification, you dont need the files yet
        foreach ($alertContacts as $emergencyContact) {
            if (true === $hasAttachableRequest) {
                $current = \get_class($this->contactAttachableRequestModel);
                $attachableRequest = new $current();
                $attachableRequest->contact()->associate($emergencyContact);
                $attachableRequest->alert()->associate($enabledAlert);
                $attachableRequest->random_id = generate_random_password(6);
                $attachableRequest->random_password = encrypt(generate_random_password(10));

                $attachableRequest->save();
            } else {
                $attachableRequest = null;
            }

            $emergencyContact->notify(new AlertNotification($emergencyContact, $enabledAlert, $enabledAlert->user, $attachableRequest));
        }

        // Set alert as sent || or set as pristine
        $enabledAlert->setAsSent($is_emergency);
    }
}
