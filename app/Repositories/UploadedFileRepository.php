<?php

namespace App\Repositories;

use App\Contracts\Repositories\UploadedFileRepositoryInterface;
use App\Jobs\ProcessUploadedFileEncryption;
use App\Models\UploadedFile;
use App\Models\UploadedFileType;
use App\Models\User;
use Illuminate\Http\UploadedFile as File;
use Ramsey\Uuid\Uuid;

class UploadedFileRepository extends Repository implements UploadedFileRepositoryInterface
{
    /**
     * Attributes.
     */
    protected $uploadedFileTypeModel;

    public function __construct(
        UploadedFile $model,
        UploadedFileType $uploadedFileTypeModel
    ) {
        $this->model = $model;
        $this->uploadedFileTypeModel = $uploadedFileTypeModel;
    }

    public function areUploadedFilesOwnedByUser(User $user, $uploadedFiles): bool
    {
        foreach ($uploadedFiles as $uploadedFile) {
            if (!$user->uploadedFiles->contains($uploadedFile)) {
                return false;
            }
        }

        return true;
    }

    public function uploadFile(File $file, User $user): UploadedFile
    {
        $original_name = $file->getClientOriginalName();
        $extension = $file->guessExtension();
        $generated_name = Uuid::uuid4();
        if (!is_dir(config('global.files.paths.temp'))) {
            mkdir(config('global.files.paths.temp'));
        }
        $path = config('global.files.paths.temp');
        $file->move($path, $generated_name->toString());

        $uploadedFile = $this->newInstance();
        $uploadedFile->fill([
            'path'              => $path,
            'extension'         => $extension,
            'original_name'     => $original_name,
            'generated_name'    => $generated_name
        ]);

        $uploadedFile->type()->associate($this->uploadedFileTypeModel->where('type', config('global.files.upload'))->first());
        $uploadedFile->user()->associate($user);
        $uploadedFile->save();

        ProcessUploadedFileEncryption::dispatch($uploadedFile->uuid)->onQueue('default');

        return $uploadedFile;
    }
}
