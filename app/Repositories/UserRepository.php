<?php

namespace App\Repositories;

use App\Contracts\Repositories\UserRepositoryInterface;
use App\Models\User;
use App\Notifications\VerifyUserContact;
use Carbon\Carbon;

class UserRepository extends Repository implements UserRepositoryInterface
{
    public function __construct(
        User $model
    ) {
        $this->model = $model;
    }

    public function isUserIdentifierExist(string $identifier): bool
    {
        return $this->getUserByIdentifier($identifier) instanceof User ? true : false;
    }

    public function getUserByIdentifier(string $identifier): ?User
    {
        return $this->model->where(['email' => $identifier])->orWhere(['phone' => $identifier])->first();
    }

    public function isVerified(User $user): bool
    {
        return null !== $user->verified_at;
    }

    public function verifyAccount(User $user, int $verification_number): bool
    {
        if ($user->hasVerifiedIdentifier()) {
            return false;
        }

        if ($user->created_at->addSeconds(config('global.users.verification.verification_expire_in')) < Carbon::now()
            || !((int) $verification_number === (int) show_pin($user->verification_number))) {
            return false;
        }

        $user->verification_number = null;
        $user->verified_at = Carbon::now();

        if (null === $user->phone) {
            $user->email_verified_at = Carbon::now();
        } else {
            $user->phone_verified_at = Carbon::now();
        }
        $user->save();

        return true;
    }

    public function verifyContact(User $user, int $verification_number): bool
    {
        if (!$user->hasVerifiedIdentifier()) {
            return false;
        }
        if (!((int) $verification_number === (int) show_pin($user->verification_number))) {
            return false;
        }

        $user->verification_number = null;
        if (null === $user->email_verified_at) {
            $user->email_verified_at = Carbon::now();
        } else {
            $user->phone_verified_at = Carbon::now();
        }
        $user->save();

        return true;
    }

    public function registerUser(array $input): ?User
    {
        // verify mandatory fields
        $mandatory_fields = [
            'password',
            'name',
            'civility'
        ];

        $conditional_fields = [
            'email',
            'phone'
        ];

        $dependant_fields = [
            'phone' => ['country']
        ];

        if (!array_keys_exists_not_empty($mandatory_fields, $input)
        || !array_key_exists_conditionals($conditional_fields, $input)
        || !array_key_dependencies($dependant_fields, $input)) {
            return null;
        }

        $input['password'] = bcrypt($input['password']);

        return $this->model->create($input);
    }

    public function addNotifiableFieldOnUpdate(User $user, string $field, string $value): bool
    {
        $notify = false;

        if (!\in_array($field, config('global.users.parameters.notifiables'), true)) {
            return false;
        }

        if ($user->$field !== $value) {
            $notify = true;
        }

        $user->$field = $value;
        $user->verification_number = generate_pin(4);
        $user->save();

        if (false === $notify) {
            return false;
        }

        $user->notify(new VerifyUserContact($user, $field));

        return true;
    }
}
