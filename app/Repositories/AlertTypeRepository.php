<?php

namespace App\Repositories;

use App\Contracts\Repositories\AlertTypeRepositoryInterface;
use App\Models\AlertType;

class AlertTypeRepository extends Repository implements AlertTypeRepositoryInterface
{
    public function __construct(
        AlertType $model
    ) {
        $this->model = $model;
    }
}
