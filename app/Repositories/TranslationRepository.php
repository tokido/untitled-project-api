<?php

namespace App\Repositories;

use App\Contracts\Repositories\TranslationRepositoryInterface;
use App\Models\Translation;

class TranslationRepository extends Repository implements TranslationRepositoryInterface
{
    public function __construct(
        Translation $model
    ) {
        $this->model = $model;
    }

    public function getCurrent(string $current)
    {
        if (!\in_array($current, array_keys(config('global.i18n.locales')), true)) {
            return null;
        }

        return $this->model->where(['locale'  => $current])->get();
    }
}
