<?php

namespace App\Repositories;

use App\Contracts\Repositories\NotificationRepositoryInterface;
use App\Models\Notification;
use Carbon\Carbon;

class NotificationRepository extends Repository implements NotificationRepositoryInterface
{
    public function __construct(
        Notification $model
    ) {
        $this->model = $model;
    }

    public function getEnabledNotifications()
    {
        // get Expired Notifications who has not been disabled
        return $this->model
            ->where('is_waiting_response', true)
            ->where('expires_at', '<', Carbon::now())
            ->where('was_disabled', false)
            ->whereNull('disabled_at')
            ->get();
    }
}
