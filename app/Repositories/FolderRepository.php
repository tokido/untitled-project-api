<?php

namespace App\Repositories;

use App\Contracts\Repositories\FolderRepositoryInterface;
use App\Models\Folder;

class FolderRepository extends Repository implements FolderRepositoryInterface
{
    public function __construct(
        Folder $model
    ) {
        $this->model = $model;
    }
}
