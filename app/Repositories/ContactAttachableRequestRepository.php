<?php

namespace App\Repositories;

use App\Contracts\Repositories\ContactAttachableRequestRepositoryInterface;
use App\Models\ContactAttachableRequest;

class ContactAttachableRequestRepository extends Repository implements ContactAttachableRequestRepositoryInterface
{
    public function __construct(
        ContactAttachableRequest $model
    ) {
        $this->model = $model;
    }

    public function getByUserNameAndVerifyPassword(string $username, string $passowrd)
    {
        $response = $this->model->where(['random_id' => $username])->first();
        if (null !== $response && decrypt($response->random_password) === (string) $passowrd) {
            return $response;
        }

        return null;
    }
}
