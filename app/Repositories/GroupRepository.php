<?php

namespace App\Repositories;

use App\Contracts\Repositories\GroupRepositoryInterface;
use App\Models\Group;
use App\Models\User;

class GroupRepository extends Repository implements GroupRepositoryInterface
{
    public function __construct(
        Group $model
    ) {
        $this->model = $model;
    }

    public function areGroupsOwnedByUser(User $user, $groups): bool
    {
        foreach ($groups as $group) {
            if (!$user->groups->contains($group)) {
                return false;
            }
        }

        return true;
    }

    public function areContactsInGroup(Group $group, $contacts): bool
    {
        foreach ($contacts as $contact) {
            if (!$group->contacts->contains($contact)) {
                return false;
            }
        }

        return true;
    }
}
