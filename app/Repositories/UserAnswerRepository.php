<?php

namespace App\Repositories;

use App\Contracts\Repositories\UserAnswerRepositoryInterface;
use App\Models\UserSecurityQuestion;

class UserAnswerRepository extends Repository implements UserAnswerRepositoryInterface
{
    public function __construct(
        UserSecurityQuestion $model
    ) {
        $this->model = $model;
    }
}
