<?php

namespace App\Repositories;

use App\Contracts\Repositories\AlertNotificationTimeRepositoryInterface;
use App\Models\AlertNotificationTime;

class AlertNotificationTimeRepository extends Repository implements AlertNotificationTimeRepositoryInterface
{
    public function __construct(
        AlertNotificationTime $model
    ) {
        $this->model = $model;
    }
}
