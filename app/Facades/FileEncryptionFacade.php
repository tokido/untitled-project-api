<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class FileEncryptionFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'fileEncryption';
    }
}
