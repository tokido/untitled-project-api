<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ApiDateFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'apiDate';
    }
}
