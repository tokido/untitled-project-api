<?php

namespace App\Facades\Services;

use App\Jobs\AdminNotifier;
use App\Models\EncryptedFile;
use App\Models\UploadedFile;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

class FileEncryption
{
    /**
     * Encrypt Saved File.
     *
     * @param UploadedFile $uploadedFile
     */
    public function encrypt(UploadedFile $uploadedFile): void
    {
        // Set encryption source information
        if (!is_dir(config('global.files.paths.encrypted'))) {
            mkdir(config('global.files.paths.encrypted'));
        }

        $key = config('global.files.encryption.key');
        $encryptedFilename = Uuid::uuid4();
        $encryptedFilename = $encryptedFilename->toString();
        $source = $uploadedFile->path . $uploadedFile->generated_name;
        $dest = config('global.files.paths.encrypted') . $encryptedFilename;

        // A little bit of salty PPD
        $key = mb_substr(sha1($key, true), 0, 16);
        $iv = openssl_random_pseudo_bytes(16);

        // save encryption time
        $uploadedFile->encryption_start = Carbon::now();

        $error = false;
        if ($fpOut = fopen($dest, 'w')) {
            // Put the initialzation vector to the beginning of the file
            fwrite($fpOut, $iv);
            if ($fpIn = fopen($source, 'r')) {
                while (!feof($fpIn)) {
                    $plaintext = fread($fpIn, 16 * config('global.files.encryption.threshold'));
                    $ciphertext = openssl_encrypt($plaintext, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $iv);
                    // Use the first 16 bytes of the ciphertext as the next initialization vector
                    $iv = mb_substr($ciphertext, 0, 16);
                    fwrite($fpOut, $ciphertext);
                }
                fclose($fpIn);
            } else {
                $error = true;
            }
            fclose($fpOut);
        } else {
            $error = true;
        }
        // save on the database
        if (true === $error) {
            AdminNotifier::dispatch(
                'encryption',
                trans('notifications.encryption.fail'),
                \get_class($uploadedFile),
                $uploadedFile->uuid
            )->onQueue('default');
        }

        $uploadedFile->is_encryption_success = true;
        $uploadedFile->encryption_end = Carbon::now();
        $uploadedFile->save();
        $ecFile = new EncryptedFile();
        $ecFile->fill([
            'path'      => config('global.files.paths.encrypted'),
            'name'      => $encryptedFilename,
            'block'     => 1,
            'size'      => 0, // size isn't used yet
        ]);

        // Delete File after upload
        unlink($source);

        $ecFile->uploadedFile()->associate($uploadedFile);
        $ecFile->save();
    }

    /**
     * Decrypt encrypted saved file.
     *
     * @param UploadedFile $uploadedFile
     */
    public function decrypt(UploadedFile $uploadedFile, $dest = null): ?UploadedFile
    {
        // save crypt

        $encryptedFile = $uploadedFile->encryptedFiles()->first();
        $source = $encryptedFile->path . $encryptedFile->name;
        $key = config('global.files.encryption.key');
        if (null === $dest) {
            $dest = config('global.files.paths.temp') . $uploadedFile->original_name;
        }

        $uploadedFile->decryption_start = Carbon::now();

        $error = false;
        // A little bit of salty PPD
        $key = mb_substr(sha1($key, true), 0, 16);

        if ($fpOut = fopen($dest, 'w')) {
            if ($fpIn = fopen($source, 'r')) {
                // Get the initialzation vector from the beginning of the file
                $iv = fread($fpIn, 16);
                while (!feof($fpIn)) {
                    $ciphertext = fread($fpIn, 16 * (config('global.files.encryption.threshold') + 1)); // we have to read one block more for decrypting than for encrypting
                    $plaintext = openssl_decrypt($ciphertext, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $iv);
                    // Use the first 16 bytes of the ciphertext as the next initialization vector
                    $iv = mb_substr($ciphertext, 0, 16);
                    fwrite($fpOut, $plaintext);
                }
                fclose($fpIn);
            } else {
                $error = true;
            }
            fclose($fpOut);
        } else {
            $error = true;
        }

        if (true === $error) {
            // Notify admin encryption failed
            AdminNotifier::dispatch(
                'decryption',
                trans('notifications.decryption.fail'),
                \get_class($uploadedFile),
                $uploadedFile->uuid
            )->onQueue('default');
        }

        $uploadedFile->is_decryption_success = true;
        $uploadedFile->decryption_end = Carbon::now();
        $uploadedFile->save();

        return $uploadedFile;
    }
}
