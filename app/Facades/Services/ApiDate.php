<?php

namespace App\Facades\Services;

use Carbon\Carbon;

class ApiDate
{
    /**
     * Convert data from api to carbon.
     *
     * @param string $date
     *
     * @return Carbon
     */
    public function formatInput(string $date): Carbon
    {
        return Carbon::createFromFormat(config('global.dates.api_input_format'), $date);
    }

    /**
     * Convert date to output format.
     *
     * @param  $date
     * @param string $format
     */
    public function formatOutput($date, string $format = 'Y-m-d H:i:s'): string
    {
        if ($date instanceof Carbon || $date instanceof \DateTime) {
            return $date->format(config('global.dates.api_output_format'));
        }

        return Carbon::createFromFormat($format, $date)->format(config('global.dates.api_output_format'));
    }
}
