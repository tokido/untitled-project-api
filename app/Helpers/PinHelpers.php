<?php

if (!function_exists('generate_pin')) {
    function generate_pin(int $length = 4): string
    {
        return (string) encrypt(str_pad((string) rand(0, pow(10, $length) - 1), $length, '0', STR_PAD_LEFT));
    }
}

if (!function_exists('show_pin')) {
    function show_pin(string $pin): int
    {
        return (int) decrypt($pin);
    }
}
