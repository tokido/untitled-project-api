<?php

if (!function_exists('verify_palindrome')) {
    function verify_palindrome(string $string): bool
    {
        if ((string) $string === strrev((string) $string)) {
            return true;
        }

        return false;
    }
}

if (!function_exists('reverse_string')) {
    function reverse_string(string $string): string
    {
        return strrev((string) $string);
    }
}
