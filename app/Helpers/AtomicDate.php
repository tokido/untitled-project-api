<?php

if (!function_exists('atomic_date')) {
    function atomic_date(int $minutes = 1, int $hours = 0)
    {
        $added_times = 0 !== $hours ? ($hours * $minutes * 60) : ($minutes * 60);

        return date(DATE_ATOM, time() + $added_times);
    }
}
