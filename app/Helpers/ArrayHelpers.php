<?php

if (!function_exists('array_keys_exists_not_empty')) {
    function array_keys_exists_not_empty(array $needles, array $haystack): bool
    {
        foreach ($needles as $needle) {
            if (!array_key_exists($needle, $haystack) || empty($haystack[$needle])) {
                return false;
            }
        }

        return true;
    }
}

if (!function_exists('array_key_exists_conditionals')) {
    function array_key_exists_conditionals(array $needles, array $haystack): bool
    {
        $oneExists = false;
        foreach ($needles as $needle) {
            if (!array_key_exists($needle, $haystack) || empty($haystack[$needle])) {
                $oneExists = true;
            }
        }

        return $oneExists;
    }
}

if (!function_exists('array_key_dependencies')) {
    function array_key_dependencies(array $needles, array $haystack): bool
    {
        foreach ($needles as $needle => $dependencies) {
            if (array_key_exists($needle, $haystack) && !empty($haystack[$needle])) {
                if (is_array($dependencies) && count($dependencies) > 0) {
                    foreach ($dependencies as $dependecy) {
                        if (!array_key_exists($dependecy, $haystack) || empty($haystack[$dependecy])) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
        }

        return true;
    }
}
