<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class EnsureAccountIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user() ||
             ($request->user() instanceof MustVerifyEmail &&
             !$request->user()->hasVerifiedIdentifier())) {
            return response()->json([
                    'success' => false,
                    'message' => trans('user.invalid.not.verified'),
                ], 403);
        }

        return $next($request);
    }
}
