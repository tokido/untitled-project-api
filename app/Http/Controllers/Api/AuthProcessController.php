<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\UserRepositoryInterface;
use App\Rules\MailOrNumber;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class AuthProcessController extends BaseController
{
    protected $userRepository;

    /**
     * Create a new controller instance.
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Verify if user exists.
     *
     * Verify if user exists by identifier, and return user uuid and client auth to use if you want to log in
     *
     * @group User Login
     *
     * @bodyParam identifier string required email or Phone number. Example: mail@example.net
     * @bodyParam country string is required if identifier is a Phone number, must be ISO3166 alpha-2 compliant. Example: FR
     *
     * @response {
     *  "status": true,
     *  "message": "response.success.users.verification",
     *  "data": {
     *     "user_exists": true,
     *     "identifier": "16fd2706-8baf-433b-82eb-8c7fada847da",
     *     "client_id": 2,
     *     "client_secret": "16fd2706-8baf-433b-82eb-8c7fada847da"
     *  }
     * }
     *
     * @param Request $request
     */
    public function findUserByIdentifier(Request $request)
    {
        $input = $request->all();
        $country = (\array_key_exists('country', $input) && isset($input['country']) && !empty($input['country'])) ? $input['country'] : null;
        $validator = Validator::make($input, [
            'identifier' => ['required', new MailOrNumber($country)],
            'country'    => [
                                'sometimes',
                                Rule::in(config('global.i18n.countries'))
                            ]
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.users.exists'), $validator->errors());
        }

        return $this->sendResponse(array_merge([
            'user_exists'   => $this->userRepository->isUserIdentifierExist($input['identifier']),
            'identifier'    => $input['identifier']
        ], config('global.users.oAuth')), trans('response.success.users.exists'));
    }

    /**
     * Log out from the current Device.
     */
    public function logout()
    {
        // Verfiy if user not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.users.logout'), [
                'logout'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }
        // Revoke auth
        auth()->user()->token()->revoke();

        return $this->sendResponse([
            'logout'    => true
        ], trans('response.success.users.logout'));
    }

    /**
     * Logout from all session.
     */
    public function logoutAll()
    {
        // Auth::logoutOtherDevices($password)
        // Verify if user logged in
        if (!aut()->check()) {
            return $this->sendError(trans('response.errors.users.logout'), [
                'logout'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        // Revoke all access but keep tracks
        \DB::table('oauth_access_tokens')
        ->where('user_id', auth()->user()->id)
        ->update([
            'revoked' => true
        ]);

        return $this->sendResponse([
            'logout'    => true
        ], trans('response.success.users.logout'));
    }
}
