<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\UserRepositoryInterface;
use App\Rules\NotPalindrome;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Validator;

class RegisterController extends BaseController
{
    protected $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Request user registration.
     *
     * @group User Registration
     *
     * @bodyParam name string required user full name. Example: Silvers Rayleigh
     * @bodyParam email string is required if phone number is not set. Example: silvers.rayleigh@op.net
     * @bodyParam phone phone is required if email is not set. Example: 33773835835
     * @bodyParam country string is required with phone, must be ISO3166 alpha-2 compliant and correspond to phone zone. Example: FR
     * @bodyParam password string required user passowrd, at least 8 character. Example: !ChangeMe!
     * @bodyParam password_confirmation string required same as password. Example: !ChangeMe!
     * @bodyParam civility string required in ['civility.mrs', 'civility.ms', 'civility.mr', 'civility.other']. Example: civility.other
     * @bodyParam locale string default is set to en_US. Example: fr_FR
     *
     * @response {
     *  "status": true,
     *  "message": "response.success.users.register",
     *  "data": {
     *     "token": true,
     *     "name": true,
     *     "uuid": "16fd2706-8baf-433b-82eb-8c7fada847da",
     *     "validation_expire_in": 3600
     *  }
     * }
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name'                  => 'required|min:3',
            'email'                 => 'required_without:phone|email|unique:users,email',
            'password'              => ['required', 'min:8', new NotPalindrome()],
            'password_confirmation' => 'required|same:password',
            'civility'              => 'required',
            'phone'                 => 'sometimes|phone:country|unique:users,phone',
            'country'               => [
                'required_with:phone',
                Rule::in(config('global.i18n.countries'))
            ]
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.error.users.register'), $validator->errors());
        }

        $user = $this->userRepository->registerUser($input);

        if (null === $user) {
            return $this->sendError(trans('response.errors.users.register'), [
                'register'    => false,
                'message'     => trans('response.errors.users.register')
            ]);
        }

        // Disable login after register
        return $this->sendResponse([
            'token'                 => $user->createToken(config('app.name'))->accessToken,
            'name'                  => $user->name,
            'uuid'                  => $user->uuid,
            'validation_expire_in'  => config('global.users.verification.verification_expire_in')
        ], trans('response.success.users.register'));
    }
}
