<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\ContactRepositoryInterface;
use App\Contracts\Repositories\GroupRepositoryInterface;
use App\Rules\IsNotLastEmergency;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class ContactController extends BaseController
{
    protected $contactRepository;

    protected $groupRepository;

    /**
     * Create a new controller instance.
     */
    public function __construct(
        ContactRepositoryInterface $contactRepository,
        GroupRepositoryInterface $groupRepository
    ) {
        $this->contactRepository = $contactRepository;
        $this->groupRepository = $groupRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.contacts.all'), [
                'contacts'  => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        return $this->sendResponse(auth()->user()->contacts->toArray(), trans('response.success.contacts.all'));
    }

    public function add(Request $request)
    {
        // Add contacts for an user
        $input = $request->all();

        // Verify if logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.contacts.add'), [
                'contacts'  => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        $validator = Validator::make($input, [
            'contacts'             => 'required|array',
            'contacts.*.name'      => 'required',
            'contacts.*.email'     => [
                                        'required_without:contacts.*.phone',
                                        'email',
                                        'distinct',
                                        Rule::unique('contacts', 'email')->where(function ($query) {
                                            return $query->where('user_id', auth()->user()->id);
                                        })
                                    ],
            'contacts.*.phone'     => [
                                            'sometimes',
                                            'required_without:contacts.*.country',
                                            'phone:contacts.*.country',
                                            'distinct',
                                            Rule::unique('contacts', 'phone')->where(function ($query) {
                                                return $query->where('user_id', auth()->user()->id);
                                            })
                                        ],
            'contacts.*.country'    => [
                                            'required_with:contacts.*.phone',
                                            Rule::in(config('global.i18n.countries'))
            ],
            'contacts.*.is_emergency'   => [
                'required',
                'boolean'
            ]
        ]);

        // Validate
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.contacts.add'), $validator->errors());
        }

        // Save Contact
        $contacts = auth()->user()->contacts()->createMany($input['contacts']);

        return $this->sendResponse($contacts->toArray(), trans('response.success.contacts.add'));
    }

    public function setEmergencyContact(Request $request)
    {
        // Add contacts for an user
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.contacts.add'), [
                'contacts'  => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        $validator = Validator::make($input, [
            'name'      => 'required',
            'email'     => [
                            'required_without:phone',
                            'email',
                            Rule::unique('contacts', 'email')->where(function ($query) {
                                return $query->where('user_id', auth()->user()->id);
                            })
                        ],
            'phone'     => [
                            'sometimes',
                            'phone:country',
                            Rule::unique('contacts', 'phone')->where(function ($query) {
                                return $query->where('user_id', auth()->user()->id);
                            })
                        ],
            'country'   => [
                'required_with:phone',
                Rule::in(config('global.i18n.countries'))
            ]
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.contacts.emergency.add'), $validator->errors());
        }

        $step = config('global.users.steps.emergency_contact');
        // Save Contact if not exists
        $user = auth()->user();
        if (false === $user->authorizedAuthSteps($step)) {
            return $this->sendError(trans('response.errors.emergency.add'), [
                'message'   => trans('response.errors.invalid-auth-steps'),
            ]);
        }

        $contact = $user->contacts()->create($input);

        // Update auth steps
        $user->updateAuthSteps($step);

        return $this->sendResponse($contact->toArray(), trans('response.success.contacts.emergency.add'));
    }

    public function delete(Request $request)
    {
        // Add contact for an user
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.contacts.add'), [
                'contacts'  => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        // Validate
        $validator = Validator::make($input, [
            'uuids'      => 'required|array',
            'uuids.*'    => 'sometimes|uuid'
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.contacts.delete'), $validator->errors());
        }

        $user = auth()->user();
        // Verify each contact before delete all | dont verify in mass with find by uuid
        $deletables = collect([]);
        foreach ($input['uuids'] as $contactUuid) {
            $contact = $this->contactRepository->findByUuid($contactUuid);
            if (null === $contact || !$user->contacts->contains($contact)) {
                return $this->sendError(trans('response.errors.contacts.delete'), [
                    'message'   => trans('response.errors.invalid-uuid'),
                    'uuid'      => $contactUuid
                ]);
            }
            $deletables->push($contact);
        }
        // Verify if there is still an emergency contact
        $deletableIds = $deletables->pluck('id');
        if (false === $this->contactRepository->countAlertEmergencyLeft($deletableIds->all(), $user)) {
            return $this->sendError(trans('response.errors.contacts.delete'), [
                'message'   => trans('response.errors.no-emergency'),
                'uuid'      => $contactUuid
            ]);
        }
        // Delete all
        foreach ($deletables as $deletableContact) {
            $deletableContact->delete();
        }

        return $this->sendResponse([
            'deleted'    => true
        ], trans('response.success.contacts.delete'));
    }

    public function update(Request $request)
    {
        // Add contact for an user
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.contacts.add'), [
                'contacts'  => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid'
        ]);
        // Validate input
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.contact.add'), $validator->errors());
        }

        $user = auth()->user();

        // verify contact
        $contact = $this->contactRepository->findByUuid($input['uuid']);
        if (null === $contact || !$user->contacts->contains($contact)) {
            return $this->sendError(trans('response.errors.contacts.update'), [
                'uuid'      => $input['uuid'],
                'message'   => trans('response.errors.invalid-uuid')
            ]);
        }

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid',
            'name'                  => [
                                            'required',
                                            Rule::unique('contacts', 'name')->ignore($contact->id)->where(function ($query) {
                                                return $query->where('user_id', auth()->user()->id);
                                            })
                                        ],
            'email'                 => [
                'required_without:phone',
                'present',
                'nullable',
                'email',
                Rule::unique('contacts', 'email')->ignore($contact->id)->where(function ($query) {
                    return $query->where('user_id', auth()->user()->id);
                })
            ],
            'phone'     => [
                'required_without:email',
                'present',
                'nullable',
                'phone:country',
                Rule::unique('contacts', 'phone')->ignore($contact->id)->where(function ($query) {
                    return $query->where('user_id', auth()->user()->id);
                })
            ],
            'country'    => [
                'required_with:phone',
                Rule::in(config('global.i18n.countries'))
            ],
            'is_emergency'  => ['required', 'boolean', new IsNotLastEmergency($contact, $user)]
        ]);

        // verify input
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.contacts.update'), $validator->errors());
        }

        // Update after validation
        $contact->fill([]); // fill array empty before update to clean existing input
        $contact->update($input);

        return $this->sendResponse($contact->toArray(), trans('response.index.success'));
    }

    public function addContactToGroups(Request $request)
    {
        // Add contact to a group for an user
        $input = $request->all();

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid',
            'groups'                => 'required|array',
            'groups.*'              => 'required|uuid'
        ]);

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.contacts.add'), [
                'contacts'  => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.contacts.to-groups'), $validator->errors());
        }

        $contact = $this->contactRepository->findByUuid($input['uuid']);
        $groups = $this->groupRepository->findManyByUuid($input['groups']);
        $user = auth()->user();

        if (null === $contact || null === $groups
            || !$user->contacts->contains($contact)
            || !$this->groupRepository->areGroupsOwnedByUser($user, $groups)) {
            return $this->sendError(trans('response.errors.contacts.to-groups'), [
                'message'   => trans('response.errors.invalid-uuid'),
                'uuid'      => $input['uuid']
            ]);
        }

        // Attach get Id or array Id, not object
        $contact->groups()->syncWithoutDetaching($groups->pluck('id'));

        return $this->sendResponse($contact->load('groups')->toArray(), trans('response.success.contacts.to-groups'));
    }

    public function removeContactFromGroups(Request $request)
    {
        // Remove contact from a groups for an user
        $input = $request->all();

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid',
            'groups'                => 'required|array',
            'groups.*'              => 'required|uuid'
        ]);

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.contacts.add'), [
                'contacts'  => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.contacts.remove-from-groups'), $validator->errors());
        }

        $user = auth()->user();
        $contact = $this->groupRepository->findByUuid($input['uuid']);
        $groups = $this->contactRepository->findManyByUuid($input['groups']);

        // Verify if contact is owned by group before detach
        if (null === $contact || null === $groups
            || !$user->groups->contains($contact)
            || !$this->groupRepository->areGroupsOwnedByUser($user, $groups)
            || !$this->contactRepository->areContactInGroups($contact, $groups)) {
            return $this->sendError(trans('response.errors.contacts.remove-from-groups'), [
                'message'   => trans('response.errors.invalid-uuid'),
                'uuid'      => $input['uuid']
            ]);
        }

        $contact->groups()->detach($groups->pluck('id'));

        return $this->sendResponse($contact->load('groups')->toArray(), trans('response.success.contacts.remove-from-groups'));
    }
}
