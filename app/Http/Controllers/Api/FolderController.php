<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\FolderRepositoryInterface;
use App\Contracts\Repositories\UploadedFileRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class FolderController extends BaseController
{
    protected $folderRepository;

    protected $uploadedFileRepository;

    /**
     * Create a new controller instance.
     */
    public function __construct(
        FolderRepositoryInterface $folderRepository,
        UploadedFileRepositoryInterface $uploadedFileRepository
    ) {
        $this->folderRepository = $folderRepository;
        $this->uploadedFileRepository = $uploadedFileRepository;
    }

    /**
     * get all folders.
     */
    public function index()
    {
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.folders.all'), [
                'folders'        => false,
                'message'        => trans('response.errors.users.authenticated')
            ]);
        }

        return $this->sendResponse(
            auth()->user()->folders()->with(['uploadedFiles'])
                ->orderBy('created_at', 'desc')
                ->get()
                ->toArray(),
            trans('response.success.folders.all')
        );
    }

    /**
     * Create folder.
     */
    public function add(Request $request)
    {
        $input = $request->all();

        // Validate if user is logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.folders.add'), [
                'folder'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        $validator = Validator::make($input, [
            'name'                  => [
                                            'required',
                                            'string',
                                            'min:3',
                                            Rule::unique('folders', 'name')->where(function ($query) {
                                                return $query->where('user_id', auth()->user()->id);
                                            })
                                        ],
            'files'                 => 'sometimes|array',
            'files.*'               => 'required_with:files|uuid'
        ]);

        // validate submitted files
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.folders.add'), $validator->errors());
        }

        $user = auth()->user();
        $folder = $this->folderRepository->newInstance();
        $folder->fill($input);
        $folder->user()->associate($user);

        $folder->save();

        if (isset($input['files']) && \is_array($input['files']) && \count($input['files']) > 0) {
            $uploadedFiles = $this->uploadedFileRepository->findManyByUuid($input['files']);
            // Then verify if files exists & are owned by the user
            if (null !== $uploadedFiles && \count($uploadedFiles) > 0) {
                if (false === $this->uploadedFileRepository->areUploadedFilesOwnedByUser($user, $uploadedFiles)) {
                    $folder->forceDelete();

                    return $this->sendError(trans('response.errors.folders.add-files'), [
                        'message'   => trans('response.errors.invalid-uuid'),
                        'uuid'      => $input['uuid']
                    ]);
                }

                $folder->uploadedFiles()->syncWithoutDetaching($uploadedFiles->pluck('id'));
            }
        }
        $folder->save();

        return $this->sendResponse($folder->load('uploadedFiles')->toArray(), trans('response.success.folders.add-uploadedFiles'));
    }

    public function delete(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid'
        ]);

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.folders.all'), [
                'folders'    => false,
                'message'    => trans('response.errors.users.authenticated')
            ]);
        }

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.folders.delete'), $validator->errors());
        }

        $folder = $this->folderRepository->findByUuid($input['uuid']);
        if (null === $folder || !auth()->user()->folders->contains($folder)) {
            return $this->sendError(trans('response.errors.folders.delete'), [
                'message'   => trans('response.errors.invalid-uuid'),
                'uuid'      => $input['uuid']
            ]);
        }

        // Delete folder, but keep files
        $folder->delete();

        return $this->sendResponse([
            'deleted'    => true
        ], trans('response.success.folders.delete'));
    }

    public function rename(Request $request)
    {
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.folders.rename'), [
                'folders'    => false,
                'message'    => trans('response.errors.users.authenticated')
            ]);
        }

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid'
        ]);
        // Validate input
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.folder.update'), $validator->errors());
        }

        $folder = $this->folderRepository->findByUuid($input['uuid']);
        if (null === $folder || !auth()->user()->folders->contains($folder)) {
            return $this->sendError(trans('response.errors.folders.delete'), [
                'message'   => trans('response.errors.invalid-uuid'),
                'uuid'      => $input['uuid']
            ]);
        }

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid',
            'name'                  => [
                                            'required',
                                            Rule::unique('folders', 'name')->ignore($folder->id)->where(function ($query) {
                                                return $query->where('user_id', auth()->user()->id);
                                            })
                                        ]
        ]);

        // Validate input
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.folders.update'), $validator->errors());
        }

        // Update folder
        $folder->update($input);

        return $this->sendResponse($folder->load(['uploadedFiles'])->toArray(), trans('response.success.folders.delete'));
    }

    public function addFilesToFolder(Request $request)
    {
        // Add contact to a folder for an user
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.folders.all'), [
                'folders'    => false,
                'message'    => trans('response.errors.users.authenticated')
            ]);
        }
        $validator = Validator::make($input, [
            'uuid'               => 'required|uuid',
            'files'              => 'required|array',
            'files.*'            => 'required|uuid'
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.folders.add-uploadedFiles'), $validator->errors());
        }

        $user = auth()->user();
        $folder = $this->folderRepository->findByUuid($input['uuid']);

        $uploadedFiles = $this->uploadedFileRepository->findManyByUuid($input['files']);

        if (null === $folder || null === $uploadedFiles
            || !$user->folders->contains($folder)
            || !$this->uploadedFileRepository->areUploadedFilesOwnedByUser($user, $uploadedFiles)) {
            // Attach get Id or array Id, not object
            return $this->sendError(trans('response.errors.folder.add-files'), [
                'message'   => trans('response.errors.invalid-uuid'),
                'uuid'      => $input['uuid']
            ]);
        }

        // Sync new uploadedFiles
        $folder->uploadedFiles()->syncWithoutDetaching($uploadedFiles->pluck('id'));

        return $this->sendResponse($folder->load('uploadedFiles')->toArray(), trans('response.success.folder.add-files'));
    }

    public function removeFilesFromFolder(Request $request)
    {
        // Remove uploadedFiles from a folder for an user
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.remove-from-folder'), [
                'folders'    => false,
                'message'    => trans('response.errors.users.authenticated')
            ]);
        }

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid',
            'files'                 => 'required|array',
            'files.*'               => 'required|uuid'
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.folders.remove-from-folder'), $validator->errors());
        }

        $user = auth()->user();
        $folder = $this->folderRepository->findByUuid($input['uuid']);
        $uploadedFiles = $this->uploadedFileRepository->findManyByUuid($input['uploadedFiles']);

        if (null === $folder || null === $uploadedFiles
            || !$user->folders->contains($folder)
            || !$this->uploadedFileRepository->areContactsOwnedByUser($user, $uploadedFiles)
            || !$this->folderRepository->areContactsInGroup($folder, $uploadedFiles)) {
            return $this->sendError(trans('response.errors.folders.remove-from-folders'), [
                'message'   => trans('response.errors.invalid-uuid'),
                'uuid'      => $input['uuid']
            ]);
        }

        // Detach from foldern no deletion
        $folder->uploadedFiles()->detach($uploadedFiles->pluck('id'));

        return $this->sendResponse($folder->load('uploadedFiles')->toArray(), trans('response.success.folders.remove-from-folders'));
    }
}
