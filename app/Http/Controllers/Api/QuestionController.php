<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\SecurityQuestionRepositoryInterface;
use App\Contracts\Repositories\UserAnswerRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Validator;

class QuestionController extends BaseController
{
    protected $securityQuestionRepository;

    protected $userAnswerRepository;

    /**
     * Create a new controller instance.
     */
    public function __construct(
        SecurityQuestionRepositoryInterface $securityQuestionRepository,
        UserAnswerRepositoryInterface $userAnswerRepository
    ) {
        $this->securityQuestionRepository = $securityQuestionRepository;
        $this->userAnswerRepository = $userAnswerRepository;
    }

    /**
     * Get security question list.
     *
     * @group User Login
     *
     * @response {
     *  "status": true,
     *  "message": "response.success.users.verification",
     *  "data": [
     *      {
     *          "uuid": "16fd2706-8baf-433b-82eb-8c7fada847da",
     *          "label": "what is your favorite cookbook"
     *      }
     *  ]
     * }
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->sendResponse($this->securityQuestionRepository->all(['is_enabled' => true])->toArray(), trans('response.success.questions.all'));
    }

    public function answer(Request $request)
    {
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.answer'), [
                'groups'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        $validator = Validator::make($input, [
            'question_uuid'      => 'required|uuid',
            'answer'             => 'required',
            'tip'                => 'required|string',
            'uuid'               => 'sometimes|uuid'
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.questions.answer'), $validator->errors());
        }
        // verify if exists
        $securityQuestion = $this->securityQuestionRepository->findByUuid($input['question_uuid']);
        $user = auth()->user();
        $step = config('global.users.steps.secret_question_valid');

        if (false === $user->authorizedAuthSteps($step)) {
            return $this->sendError(trans('response.errors.questions.answer'), [
                'message'   => trans('response.errors.invalid-auth-steps'),
                'uuid'      => $input['question_uuid']
            ]);
        }
        // verify current Auth-steps :

        if (null === $securityQuestion) {
            return $this->sendError(trans('response.errors.questions.answer'), [
                'message'   => trans('response.errors.invalid-uuid'),
                'uuid'      => $input['question_uuid']
            ]);
        }

        if (\array_key_exists('uuid', $input) && !empty($input['uuid'])) {
            unset($input['question_uuid']);
            $securityQuestionAnswer = $this->userAnswerRepository->findByUuid($input['uuid']);
            if (null !== $securityQuestionAnswer && $user->userSecurityQuestion->is($securityQuestionAnswer)) {
                $securityQuestionAnswer->fill(array_filter($input))
                    ->securityQuestion()->associate($securityQuestion);
            }
        } else {
            unset($input['question_uuid']);
            // Prevent to have multiple security question answer
            $securityQuestionAnswer = $this->userAnswerRepository->newInstance();
            $securityQuestionAnswer->fill(array_filter($input))
                                   ->user()->associate($user)
                                   ->securityQuestion()
                                   ->associate($securityQuestion);
            // Update auth steps
            $user->updateAuthSteps($step);
        }
        $securityQuestionAnswer->save();

        return $this->sendResponse($securityQuestionAnswer->load(['securityQuestion'])->toArray(), trans('response.success.questions.answer'));
    }

    public function getCurrent()
    {
        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.current'), [
                'groups'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        return $this->sendResponse(
            auth()->user()->userSecurityQuestion->load(['securityQuestion'])
            ->toArray(),
            trans('response.success.questions.current')
        );
    }
}
