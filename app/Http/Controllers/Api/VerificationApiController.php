<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class VerificationApiController extends BaseController
{
    protected $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Validate user's identifier.
     *
     * validate user identifier (email or Phone number) with the verification number sent to him
     *
     * @group User Registration
     *
     * @bodyParam uuid uuid required User universal unique identifier. Example: 16fd2706-8baf-433b-82eb-8c7fada847da
     * @bodyParam verification_number int required verification number sent to user's phone or email. Example: 1234
     *
     * @response {
     *  "status": true,
     *  "message": "response.success.users.verification",
     *  "data": {
     *     "user_exists": true,
     *     "validation": true,
     *     "identifier": "16fd2706-8baf-433b-82eb-8c7fada847da"
     *  }
     * }
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyAccount(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'uuid'                    => 'required|uuid',
            'verification_number'     => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.users.verification'), $validator->errors());
        }
        $user = $this->userRepository->findByUuid($input['uuid']);

        // check if user valid
        if (null === $user) {
            return $this->sendError(trans('response.errors.users.verification'), [
                'message'       => trans('response.errors.invalid-uuid'),
                'uuid'          => $input['uuid']
            ]);
        }

        // If already verified
        if (true === $this->userRepository->isVerified($user)) {
            return $this->sendError(trans('response.errors.users.verification'), [
                'user_exists'   => true,
                'message'       => trans('response.errors.users.already-verified'),
                'uuid'          => $input['uuid']
            ]);
        }
        $step = config('global.users.steps.identifier_valid');

        if (false === $user->authorizedAuthSteps($step)) {
            return $this->sendError(trans('response.errors.verification.add'), [
                'message'   => trans('response.errors.invalid-auth-steps'),
            ]);
        }

        if (!$this->userRepository->verifyAccount($user, $input['verification_number'])) {
            return $this->sendError(trans('response.errors.users.pin-expired'), [
                'user_exists'   => true,
                'validation'    => false,
                'uuid'          => $input['uuid']
            ]);
        }

        // Update first login step
        $user->updateAuthSteps($step);

        return $this->sendResponse([
            'user_exists'   => true,
            'validation'    => true,
            'identifier'    => $input['uuid']
        ], trans('response.success.users.verification'));
    }

    /**
     * Verify user contact added by an user.
     *
     * Request files download for a contact (non logged in user)
     *
     * @group User verification
     */
    public function verifyContact(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'verification_number'     => 'required|integer',
        ]);

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.validations'), [
                'groups'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.users.verification'), $validator->errors());
        }

        $user = auth()->user();

        if (!$user->hasVerifiedIdentifier()) {
            return $this->sendError(trans('response.errors.users.pin-expired'), [
                'user_exists'   => true,
                'validation'    => false,
                'identifier'    => $user->uuid
            ]);
        }
        if (!$this->userRepository->verifyContact($user, $input['verification_number'])) {
            return $this->sendError(trans('response.generic.verify.validations'), [
                'user_exists'   => true,
                'validation'    => false,
                'uuid'          => $user->uuid
            ]);
        }

        return $this->sendResponse([
            'user_exists'   => true,
            'validation'    => true,
            'identifier'    => $user->uuid
        ], trans('response.success.users.verification'));
    }
}
