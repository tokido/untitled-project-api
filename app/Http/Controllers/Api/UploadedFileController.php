<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\UploadedFileRepositoryInterface;
use FileEncryption;
use Illuminate\Http\Request;
use Validator;

class UploadedFileController extends BaseController
{
    protected $uploadedFileRepository;

    public function __construct(
        UploadedFileRepositoryInterface $uploadedFileRepository
    ) {
        $this->uploadedFileRepository = $uploadedFileRepository;
    }

    public function upload(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'uploaded_files'            => 'required|array',
            'uploaded_files.*'          => 'mimes:' . config('global.files.mimes') . '|max:100000' // .config('global.files.upload.max') // Authorized_files
        ]);

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.files.upload'), [
                'upload'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        // Validate input
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.files.upload'), $validator->errors());
        }

        // save each file
        $uploadedFilesList = [];

        foreach ($input['uploaded_files'] as $file) {
            array_push($uploadedFilesList, $this->uploadedFileRepository->uploadFile($file, auth()->user()));
        }

        return $this->sendResponse($uploadedFilesList, trans('response.success.files.upload'));
    }

    public function download(Request $request)
    {
        $input = $request->all();
        $rules = [
            'uuid'            => 'required|uuid'
        ];

        if (!auth()->check()) {
            $rules['security_check'] = 'required|string|min:4';
        }

        $validator = Validator::make($input, $rules);

        // Validate input
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.files.download'), $validator->errors());
        }

        //Verify Uuid
        // get alert if exists
        $uploadedFile = $this->uploadedFileRepository->findByuuid($input['uuid']);
        if (null === $uploadedFile) {
            return $this->sendError([
                'uuid'      => $input['uuid'],
                'message'   => trans('response.errors.invalid-uuid')
            ], trans('response.errors.files.download'));
        }

        $decryptedFile = FileEncryption::decrypt($uploadedFile);
        if (null === $decryptedFile) {
            return $this->sendError([
                'uuid'      => $input['uuid'],
                'message'   => trans('response.errors.invalid-uuid')
            ], trans('response.errors.files.download'));
        }

        return $this->sendResponse($decryptedFile->toArray(), trans('response.success.files.upload'));
    }

    public function index()
    {
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.files.all'), [
                'uploadedFiles'  => false,
                'message'        => trans('response.errors.users.authenticated')
            ]);
        }

        return $this->sendResponse(
            auth()->user()->uploadedFiles()->with(['folders'])
                ->orderBy('created_at', 'desc')
                ->get()
                ->toArray(),
            trans('response.success.files.all')
        );
    }
}
