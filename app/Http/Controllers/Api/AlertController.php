<?php

namespace App\Http\Controllers\Api;

use Alphametric\Validation\Rules\LocationCoordinates;
use App\Contracts\Repositories\AlertNotificationTimeRepositoryInterface;
use App\Contracts\Repositories\AlertRepositoryInterface;
use App\Contracts\Repositories\AlertTypeRepositoryInterface;
use App\Contracts\Repositories\ContactAttachableRequestRepositoryInterface;
use App\Jobs\AdminNotifier;
use App\Jobs\PrepareAttachableDownload;
use App\Rules\CreatableAlert;
use App\Rules\IntervalIsValid;
use App\Rules\IsHourValid;
use App\Rules\OnlyOneIsRequired;
use App\Rules\ProgrammedAlert;
use App\Rules\RequiredIfBlackmail;
use App\Rules\RequiredIfProgrammable;
use App\Rules\UpdatableAlert;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Validator;

class AlertController extends BaseController
{
    protected $alertRepository;

    protected $alertNotificationTimeRepository;

    protected $alertTypeRepository;

    protected $contactAttachableRequestRepository;

    /**
     * Create new controller instance and inject repository.
     */
    public function __construct(
        AlertRepositoryInterface $alertRepository,
        AlertNotificationTimeRepositoryInterface $alertNotificationTimeRepository,
        AlertTypeRepositoryInterface $alertTypeRepository,
        ContactAttachableRequestRepositoryInterface $contactAttachableRequestRepository
    ) {
        $this->alertRepository = $alertRepository;
        $this->alertNotificationTimeRepository = $alertNotificationTimeRepository;
        $this->alertTypeRepository = $alertTypeRepository;
        $this->contactAttachableRequestRepository = $contactAttachableRequestRepository;
    }

    const MORPHABLES = ['attachables', 'alertables'];
    const ATTACHABLES = [
        'name'  => 'attachables',
        'rule'  => ['file', 'folder'],
        'load'  => ['uploadedFiles', 'folders']
    ];
    const ALERTABLES = [
        'name'  => 'alertables',
        'rule'  => ['contact', 'group'],
        'load'  => ['contacts', 'groups']
    ];

    /**
     * Get all alerts related to logged in user.
     */
    public function index()
    {
        //verify if user is logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.alerts.all'), [
                'alerts'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        // Get user alert
        return $this->sendResponse(auth()->user()
            ->alerts()
            ->with([
                'type',
                'contacts',
                'groups',
                'folders',
                'uploadedFiles',
                'alertNotificationTimes'
            ])->get()->toArray(), trans('response.success.alerts.all'));
    }

    /**
     * Set an Alert.
     */
    public function create(Request $request)
    {
        return $this->createOrUpdateAlert($request, false);
    }

    /**
     * Update existing alert By uuid.
     */
    public function update(Request $request)
    {
        return $this->createOrUpdateAlert($request, true);
    }

    /**
     * Delete existing alert.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $input = $request->all();

        // Return error if user not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.alerts.add'), [
                'alerts'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }
        $validator = Validator::make($input, [
            'uuid'  => 'required|uuid'
        ]);

        // Return error if exists
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.alerts.delete'), $validator->errors());
        }

        // get alert if exists
        $alert = $this->alertRepository->findByuuid($input['uuid']);

        if (null === $alert) {
            return $this->sendError([
                'uuid'      => $input['uuid'],
                'message'   => trans('response.errors.invalid-uuid')
            ], trans('response.errors.alerts.delete'));
        }

        // verify if alert is Updatable / Deletable
        if (false === $alert->isDeletable()) {
            return $this->sendError([
                'uuid'      => $input['uuid'],
                'message'   => trans('response.errors.alerts.not-deletable')
            ], trans('response.errors.alerts.not-deletable'));
        }

        $alert->delete();

        return $this->sendResponse([
            'deleted'    => true,
        ], trans('response.success.alerts.delete'));
    }

    public function emergency(Request $request)
    {
        $input = $request->all();

        // Device and position informations || no validation for now
        $validator = Validator::make($input, [
            'device'                => 'required',
            'location_coordinates'  => ['sometimes', new LocationCoordinates()]
        ]);

        if (!auth()->check()) {
            // Notify admin someone tried to send an emergency alert
            AdminNotifier::dispatch(
                'alert',
                trans('notifications.emergency.fail'),
                'alert',
                null
            )->onQueue('default');
            // Return;
            return $this->sendError(trans('response.errors.alerts.emergency'), [
                'alerts'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        $user = auth()->user();
        // Get user emergencies alert, It can't be empty
        $emergencies = $this->alertRepository->getUserAlertsByType($user);
        foreach ($emergencies as $emergencyAlert) {
            $emergencyAlert->programmed_at = Carbon::now();
            $emergencyAlert->location_coordinates = (isset($input['location_coordinates']) && !empty($input['location_coordinates'])) ? $input['location_coordinates'] : null;
            $emergencyAlert->save();
        }

        return $this->sendResponse([
            'emergency' => true,
            'alerts'    => $emergencies->toArray()
        ], trans('response.success.alerts.add'));
    }

    protected function createOrUpdateAlert(Request $request, bool $update = false)
    {
        $input = $request->all();

        // Return error if user not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.alerts.add'), [
                'alerts'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        // Two Steps Validation
        $updateInitRules = [
            'uuid'              => 'required|uuid'
        ];

        // Verify if uuid & alert_type_uuid exists first
        if (true === $update) {
            // Verify Uuid only
            $validator = Validator::make($input, $updateInitRules);
            // Return error if exists
            if ($validator->fails()) {
                return $this->sendError(trans('response.errors.alerts.add'), $validator->errors());
            }
            // Get alert if exists
            $alert = $this->alertRepository->findByUuid($input['uuid']);
            if (null === $alert) {
                return $this->sendError(trans('response.errors.alerts.update'), [
                    'uuid'      => $input['uuid'],
                    'message'   => trans('response.errors.invalid-uuid')
                ]);
            }

            $updateInitRules['alert_type_uuid'] = ['required', 'uuid', new UpdatableAlert($alert)];
            $validator = Validator::make($input, $updateInitRules);
            // Return error if exists
            if ($validator->fails()) {
                return $this->sendError(trans('response.errors.alerts.add'), $validator->errors());
            }

            // Add to global rules
            $rules['alert_type_uuid'] = ['required', 'uuid', new UpdatableAlert($alert)];
            $rules['uuid'] = ['required', 'uuid'];
        } else {
            $rules['alert_type_uuid'] = ['required', 'uuid', new CreatableAlert()];
        }

        // Get Alert Type
        // Check if alert type uuid is valid
        $alertType = $this->alertTypeRepository->findByUuid($input['alert_type_uuid']);
        if (null === $alertType) {
            return $this->sendError(trans('response.errors.alerts.add'), [
                'uuid'      => $input['alert_type_uuid'],
                'message'   => trans('response.errors.invalid-uuid')
            ]);
        }

        /**
         * Get the alert type to validate:
         * Notification interval
         * Programmed_at (if programmed Alert).
         */
        $programmed_at = (isset($input['programmed_at']) && !empty($input['programmed_at'])) ? $input['programmed_at'] : null;
        $rules = array_merge($rules, [
            'title'                             => ['sometimes', 'required', 'min:4'],
            'message'                           => ['required', 'min:8'],
            'location_coordinates'              => ['sometimes', new LocationCoordinates()],
            'is_draft'                          => ['sometimes', 'boolean'],
            'is_enabled'                        => ['sometimes', 'boolean'],
            'programmed_at'                     => [
                new ProgrammedAlert($alertType),
                'required_without:notification_recurrences',
                'date_format:' . config('global.dates.api_input_format'),
                'after:' . atomic_date(5)
            ],
            'notification_interval'             => [
                new OnlyOneIsRequired(array_keys($input), ['notification_times']),
                'integer',
                new ProgrammedAlert($alertType),
                new RequiredIfProgrammable($alertType),
                new IntervalIsValid($alertType, false, $programmed_at)
            ],
            'notification_times'                => [
                new OnlyOneIsRequired(array_keys($input), ['notification_interval']),
                new ProgrammedAlert($alertType),
                new RequiredIfProgrammable($alertType),
                'array'
            ],
            'notification_times.*.notify_at'    => [
                'required_with:notification_times',
                'date_format:' . config('global.dates.api_input_format'),
                'after:' . atomic_date(5),
                new IntervalIsValid($alertType, true, $programmed_at)
            ],
            'notification_times.*.is_recurrent'       => 'sometimes|boolean',
            'notification_recurrences'                => [
                new OnlyOneIsRequired(array_keys($input), ['notification_interval', 'notification_times']),
                new RequiredIfBlackmail($alertType),
                // 'required_without:programmed_at,notification_interval,notification_times',
                'array'
            ],
            'notification_recurrences.*.day_of_week'    => [
                'required_with:notification_recurrences',
                'integer',
                Rule::in(config('global.alert.days_of_week'))
            ],
            'notification_recurrences.*.hour'           => [
                'required_with:notification_recurrences',
                'string',
                new IsHourValid()
            ],
            'notification_recurrences.*.is_recurrent' => 'sometimes|boolean'
        ]);

        // Validate input
        $validator = Validator::make($input, $rules);

        // Return error if exists
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.alerts.add'), $validator->errors());
        }

        // Check if alert exists
        if (false === $update) {
            $alert = $this->alertRepository->newInstance();
            $alert->fill($input);
        } else {
            $alert->update($input);
        }

        // Associate
        $alert->user()->associate(auth()->user());
        $alert->type()->associate($alertType);

        // Save alert
        $alert->save();
        // Delete existing notification times
        $alert->alertNotificationTimes()->delete();
        if (isset($input['notification_times'])
            && \is_array($input['notification_times'])
            && \count($input['notification_times']) > 0
        ) {
            $alert->alertNotificationTimes()->createMany($input['notification_times']);
        }
        // Delete existing notification recurrences
        $alert->alertNotificationRecurrences()->delete();
        if (isset($input['notification_recurrences'])
            && \is_array($input['notification_recurrences'])
            && \count($input['notification_recurrences']) > 0
        ) {
            $alert->alertNotificationRecurrences()->createMany($input['notification_recurrences']);
        }

        return $this->sendResponse($alert->load(['alertNotificationTimes', 'alertNotificationRecurrences'])->toArray(), trans('response.success.alerts.add'));
    }

    protected function validateAlertStateChange(Request $request)
    {
        $input = $request->all();
        $response = [];
        // Return error if user not logged in
        if (!auth()->check()) {
            $response['status'] = false;
            $response['response'] = $this->sendError(trans('response.errors.alerts.lists'), [
                'alerts'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);

            return $response;
        }

        $validator = Validator::make($input, [
            'uuid'                => 'required|uuid'
        ]);

        // Return error if exists
        if ($validator->fails()) {
            $response['status'] = false;
            $response['response'] = $this->sendError(trans('response.errors.alerts.disable'), $validator->errors());

            return $response;
        }
        // Get alert if exists
        $alert = $this->alertRepository->findByUuid($input['uuid']);

        if (null === $alert) {
            $response['status'] = false;
            $response['response'] = $this->sendError(trans('response.errors.alerts.disable'), [
                'uuid'      => $input['uuid'],
                'message'   => trans('response.errors.invalid-uuid')
            ]);

            return $response;
        }

        return [
            'status'    => true,
            'response'  => $alert
        ];
    }

    public function disable(Request $request)
    {
        $state = $this->validateAlertStateChange($request);
        if (false === $state['status']) {
            return $state['response'];
        }

        $alert = $state['response'];
        $alert->setAsDisabled();

        return $this->sendResponse($alert->load(['alertNotificationTimes', 'alertNotificationRecurrences'])->toArray(), trans('response.success.alerts.disable'));
    }

    public function suspend(Request $request)
    {
        $state = $this->validateAlertStateChange($request);
        if (false === $state['status']) {
            return $state['response'];
        }

        $alert = $state['response'];
        $alert->setAsSuspended();

        return $this->sendResponse($alert->load(['alertNotificationTimes', 'alertNotificationRecurrences'])->toArray(), trans('response.success.alerts.disable'));
    }

    public function enable(Request $request)
    {
        $state = $this->validateAlertStateChange($request);
        if (false === $state['status']) {
            return $state['response'];
        }

        $alert = $state['response'];
        $alert->setAsEnabled();

        return $this->sendResponse($alert->load(['alertNotificationTimes', 'alertNotificationRecurrences'])->toArray(), trans('response.success.alerts.disable'));
    }

    public function draft(Request $request)
    {
        $state = $this->validateAlertStateChange($request);
        if (false === $state['status']) {
            return $state['response'];
        }

        $alert = $state['response'];
        $alert->setAsDraft();

        return $this->sendResponse($alert->load(['alertNotificationTimes', 'alertNotificationRecurrences'])->toArray(), trans('response.success.alerts.disable'));
    }

    public function types()
    {
        // Return error if user not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.alerts.lists'), [
                'alerts'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        return $this->sendResponse($this->alertTypeRepository->all()->toArray(), trans('response.success.alerts.lists'));
    }

    public function addAttachablesToAlert(Request $request)
    {
        return $this->addMorphablesToAlert($request, self::ATTACHABLES);
    }

    public function removeAttachablesFromAlert(Request $request)
    {
        return $this->removeMorphablesFromAlert($request, self::ATTACHABLES);
    }

    public function addAlertablesToAlert(Request $request)
    {
        return $this->addMorphablesToAlert($request, self::ALERTABLES);
    }

    public function removeAlertablesFromAlert(Request $request)
    {
        return $this->removeMorphablesFromAlert($request, self::ALERTABLES);
    }

    /**
     * Get the download link if it is ready.
     *
     * Download files for a contact (non logged in user)
     *
     * @group File requests
     *
     * @bodyParam uuid uuid required alert universal Identifier. Example: 16fd2706-8baf-433b-82eb-8c7fada847da
     * @bodyParam password string  is required if user is not logged in. Example: a23b4Tc
     *
     *
     * @response {
     *  "status": true,
     *  "message": "response.success.users.verification",
     *  "data":
     *    {
     *     "uuid": "16fd2706-8baf-433b-82eb-8c7fada847da",
     *     "url": "http://local.untitled.api/download/zip/file.zip"
     *    }
     * }
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function downloadZippedFiles(Request $request)
    {
        $input = $request->all();
        $now = Carbon::now();
        $rules = [
            'username'  => 'required|string',
            'password'  => 'required|string'
        ];

        // validate request
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.request-download'), $validator->errors());
        }
        // get attachableRequest
        $attachableRequest = $this->contactAttachableRequestRepository->getByUserNameAndVerifyPassword($input['username'], $input['password']);
        if (null === $attachableRequest) {
            return $this->sendError(trans('response.errors.request-download'), [
                'alert'      => false,
                'message'    => trans('response.errors.invalid-username-password')
            ]);
        }
        $alert = $attachableRequest->alert;

        if (false === $attachableRequest->is_download_ready
            || (null === $attachableRequest->expires_at ||
                true === $attachableRequest->expires_at->greaterThan($now))
        ) {
            return $this->sendError(trans('response.errors.request-expires'), [
                'alert'      => false,
                'message'    => trans('response.errors.invalid-username-expires')
            ]);
        }

        $attachableRequest->setContactRequestDownload();

        // check if file is_still here
        $zipName = $alert->getZipPath();
        if (!file_exists($zipName)) {
            return $this->sendError(trans('response.errors.request-download'), [
                'uuid'      => $alert->id,
                'message'   => trans('response.errors.invalid-uuid')
            ]);
        }

        return $this->sendResponse([
            'uuid'  => $alert->uuid,
            'link'  => config('app.url') . '/temp/' . $alert->uuid . '.zip',
        ], trans('response.success.alerts.lists'));
    }

    /**
     * Request a download for a contact who received an alert.
     *
     * Request files download for a contact (non logged in user)
     *
     * @group File requests
     *
     * @bodyParam uuid uuid required alert universal Identifier. Example: 16fd2706-8baf-433b-82eb-8c7fada847da
     * @bodyParam password string  is required if user is not logged in. Example: a23b45T
     *
     *
     * @response {
     *  "status": true,
     *  "message": "response.success.users.verification",
     *  "data": [
     *    {
     *     "uuid": "16fd2706-8baf-433b-82eb-8c7fada847da",
     *     "original_name": "file",
     *     "mime_type": "jpeg"
     *    }
     *  ]
     * }
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function requestAttachablesDownload(Request $request)
    {
        $input = $request->all();
        $rules = [
            'username'  => 'required|string',
            'password'  => 'required|string'
        ];

        // validate request
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.request-download'), $validator->errors());
        }
        // get attachableRequest
        $attachableRequest = $this->contactAttachableRequestRepository->getByUserNameAndVerifyPassword($input['username'], $input['password']);
        if (null === $attachableRequest) {
            return $this->sendError(trans('response.errors.request-download'), [
                'uuid'      => false,
                'message'   => trans('response.errors.invalid-username-password')
            ]);
        }
        $alert = $attachableRequest->alert;
        if (null === $alert) {
            return $this->sendError(trans('response.errors.request-download'), [
                'alert'     => false,
                'message'   => trans('response.errors.invalid-uuid')
            ]);
        }

        // get all related folders and file
        $relatedFolderFiles = $this->alertRepository->requestDownloadAttachables($alert);
        // save contact request
        $attachableRequest->saveContactRequest();
        // Prepare Download & zip
        PrepareAttachableDownload::dispatch($alert->uuid, $attachableRequest->uuid)->onQueue('default');

        return $this->sendResponse($relatedFolderFiles->toArray(), trans('response.success.alerts.lists'));
    }

    public function addMorphablesToAlert(Request $request, array $morphable)
    {
        $validatedMorphable = $this->validateMorphableRequest($request, $morphable, 'add');
        if (false === $validatedMorphable['valid']) {
            return $validatedMorphable['response'];
        }
        foreach ($validatedMorphable['morphables'] as $morphableToSync) {
            $morphableToSync->alerts()->syncWithoutDetaching([
                $validatedMorphable['alert']->id => ['updated_at' => Carbon::now()]
            ]);
        }

        return $this->sendResponse($validatedMorphable['alert']->load($morphable['load'])->toArray(), trans('response.success.alerts.lists'));
    }

    public function removeMorphablesFromAlert(Request $request, array $morphable)
    {
        $validatedMorphable = $this->validateMorphableRequest($request, $morphable, 'delete');
        if (false === $validatedMorphable['valid']) {
            return $validatedMorphable['response'];
        }

        foreach ($validatedMorphable['morphables'] as $morphableToSync) {
            $morphableToSync->alerts()->detach($validatedMorphable['alert']);
        }

        return $this->sendResponse($validatedMorphable['alert']->load($morphable['load'])->toArray(), trans('response.success.alerts.lists'));
    }

    public function validateMorphableRequest(Request $request, array $morphable, string $action = 'add')
    {
        $input = $request->all();

        if (!\in_array($morphable['name'], self::MORPHABLES, true)) {
            return [
                'valid'     => false,
                'response'  => $this->sendError(trans('response.errors.morphables.types'), [
                    'alerts'    => false,
                    'message'   => trans('response.errors.morphables.types')
                ])
            ];
        }
        if (!auth()->check()) {
            return [
                'valid'     => false,
                'response'  => $this->sendError(trans('response.errors.' . $morphable['name'] . '.' . $action), [
                    'alerts'    => false,
                    'message'   => trans('response.errors.users.authenticated')
                ])
            ];
        }

        $rules = [
            'uuid'                           => 'required|uuid',
            $morphable['name']               => 'required|array',
            $morphable['name'] . '.*.uuid'   => 'required|uuid',
            $morphable['name'] . '.*.type'   => ['required', 'string', Rule::in($morphable['rule'])]
        ];

        // set rules type by attachable type
        $validator = Validator::make($input, $rules);

        // Return error if exists
        if ($validator->fails()) {
            return [
                'valid'     => false,
                'response'  => $this->sendError(trans('response.errors.' . $morphable['name'] . '.' . $action), $validator->errors())
            ];
        }

        // Verify Alert
        $alert = $this->alertRepository->findByUuid($input['uuid']);
        if (null === $alert) {
            return [
                'valid'     => false,
                'response'  => $this->sendError(trans('response.errors.' . $morphable['name'] . '.' . $action), [
                    'uuid'      => $input['uuid'],
                    'message'   => trans('response.errors.invalid-uuid')
                ])
            ];
        }

        // Verify morphables
        $morphables = $this->alertRepository->verifyAndGetMorphables($input[$morphable['name']], auth()->user());
        if (null === $morphables) {
            return [
                'valid'     => false,
                'response'  => $this->sendError(trans('response.errors.' . $morphable['name'] . '.' . $action), [
                    'attachables'   => $input[$morphable['name']],
                    'message'       => trans('response.errors.invalid-uuid')
                ])
            ];
        }

        return [
            'valid'      => true,
            'morphables' => $morphables,
            'alert'      => $alert
        ];
    }
}
