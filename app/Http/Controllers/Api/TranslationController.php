<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\TranslationRepositoryInterface;

class TranslationController extends BaseController
{
    protected $translationRepository;

    public function __construct(
        TranslationRepositoryInterface $translationRepository
    ) {
        $this->translationRepository = $translationRepository;
    }

    /**
     * Translation lists.
     *
     * @group Translation
     *
     * @queryParam locale locale. Example: en_US
     *
     * @response {
     *      "success":true,
     *      "data": {
     *          "current":"en_US",
     *          "locales":["fr_FR","en_US"],
     *          "choice":{
     *              "fr_FR":{"fr_FR":"Fran\u00e7ais","en_US":"French"},
     *              "en_US":{"fr_FR":"Anglais (Americain)","en_US":"English (American)"}
     *          },
     *          "translations":{
     *             "civility.other": {
     *                  "locale":"en_US",
     *                  "key":"civility.other",
     *                  "translated":"Other"
     *              }
     *          }
     *      },
     *      "message":"response.success.translations"
     * }
     *
     * @param string|null $locale
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrrent($locale = null)
    {
        // Get Translations in Database
        if (!isset($locale) || empty($locale) || !\in_array($locale, array_keys(config('global.i18n.locales')), true)) {
            $locale = config('app.fallback_locale');
        }

        return $this->sendResponse([
            'current'               => $locale,
            'locales'               => array_keys(config('global.i18n.locales')),
            'choice'                => config('global.i18n.locales'),
            'translations'          => $this->translationRepository->getCurrent($locale)->keyBy('key')->toArray()
        ], trans('response.success.translations'));
    }
}
