<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class UserParameterController extends BaseController
{
    const EMAIL = 'email';
    const PHONE = 'phone';

    protected $userRepository;

    /**
     * Create a new controller instance.
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Add Email to existing user.
     */
    public function addEmail(Request $request)
    {
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.current'), [
                'groups'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        $validator = Validator::make($input, [
            'email' => 'required|email|unique:users,email',
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.users.email.add'), $validator->errors());
        }

        // Add notifiable from user
        $this->userRepository->addNotifiableFieldOnUpdate(auth()->user(), self::EMAIL, $input['email']);

        return $this->sendResponse([
            self::EMAIL    => $input[self::EMAIL]
        ], trans('response.success.users.email.add'));
    }

    /**
     * Add Email to existing user.
     */
    public function addPhone(Request $request)
    {
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.current'), [
                'groups'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        // validate field
        $validator = Validator::make($input, [
            'phone'      => ['required|phone:country|unique:users,phone'],
            'country'    => [
                'required_with:phone',
                Rule::in(config('global.i18n.countries'))
            ]
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.phone.add'), $validator->errors());
        }

        // Add notifiable phone
        $this->userRepository->addNotifiableFieldOnUpdate(auth()->user(), self::PHONE, $input[self::PHONE]);

        return $this->sendResponse([
            self::PHONE    => $input[self::PHONE]
        ], trans('response.success.phone.add'));
    }
}
