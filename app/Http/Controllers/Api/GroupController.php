<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\ContactRepositoryInterface;
use App\Contracts\Repositories\GroupRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class GroupController extends BaseController
{
    protected $groupRepository;

    protected $contactRepository;

    /**
     * Create a new controller instance.
     */
    public function __construct(
        GroupRepositoryInterface $groupRepository,
        ContactRepositoryInterface $contactRepository
    ) {
        $this->groupRepository = $groupRepository;
        $this->contactRepository = $contactRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.groups.all'), [
                'groups'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        return $this->sendResponse(auth()->user()->groups()->with(['contacts'])->get()->toArray(), trans('response.success.groups.all'));
    }

    public function add(Request $request)
    {
        // Add group for an user
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.groups.all'), [
                'groups'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        $validator = Validator::make($input, [
            'name'                  => [
                                            'required',
                                            Rule::unique('groups', 'name')->where(function ($query) {
                                                return $query->where('user_id', auth()->user()->id);
                                            })
                                        ],
            'is_emergency'          => ['required', 'boolean']
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.groups.add'), $validator->errors());
        }

        // Save group
        $group = auth()->user()->groups()->create($input);

        return $this->sendResponse($group->toArray(), trans('response.success.groups.add'));
    }

    public function delete(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid'
        ]);

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.groups.all'), [
                'groups'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.groups.delete'), $validator->errors());
        }

        $group = $this->groupRepository->findByUuid($input['uuid']);
        if (null === $group || !auth()->user()->groups->contains($group)) {
            return $this->sendError(trans('response.errors.groups.delete'), [
                'message'   => trans('response.errors.invalid-uuid'),
                'uuid'      => $input['uuid']
            ]);
        }

        // Delete group
        $group->delete();

        return $this->sendResponse([
            'deleted'    => true
        ], trans('response.success.groups.delete'));
    }

    public function update(Request $request)
    {
        // Add contact for an user
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.groups.all'), [
                'groups'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid'
        ]);
        // Validate input
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.groups.update'), $validator->errors());
        }

        $group = $this->groupRepository->findByUuid($input['uuid']);
        if (null === $group || !auth()->user()->groups->contains($group)) {
            return $this->sendError(trans('response.errors.groups.delete'), [
                'message'   => trans('response.errors.invalid-uuid'),
                'uuid'      => $input['uuid']
            ]);
        }

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid',
            'name'                  => [
                                            'required',
                                            Rule::unique('groups', 'name')->ignore($group->id)->where(function ($query) {
                                                return $query->where('user_id', auth()->user()->id);
                                            })
                                        ],
            'is_emergency'          => ['required', 'boolean']
        ]);

        // Validate input
        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.groups.update'), $validator->errors());
        }

        // Update group
        $group->update($input);

        return $this->sendResponse($group->load(['contacts'])->toArray(), trans('response.success.groups.delete'));
    }

    public function addContactsToGroup(Request $request)
    {
        // Add contact to a group for an user
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.groups.all'), [
                'groups'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }
        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid',
            'contacts'              => 'required|array',
            'contacts.*'            => 'required|uuid'
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.groups.add-contacts'), $validator->errors());
        }

        $user = auth()->user();
        $group = $this->groupRepository->findByUuid($input['uuid']);
        $contacts = $this->contactRepository->findManyByUuid($input['contacts']);

        if (null === $group || null === $contacts
            || !$user->groups->contains($group)
            || !$this->contactRepository->areContactsOwnedByUser($user, $contacts)) {
            // Attach get Id or array Id, not object
            return $this->sendError(trans('response.errors.groups.add-contacts'), [
                'message'   => trans('response.errors.invalid-uuid'),
                'uuid'      => $input['uuid']
            ]);
        }

        // Sync new contacts
        $group->contacts()->syncWithoutDetaching($contacts->pluck('id'));
        $group->touch();

        return $this->sendResponse($group->load('contacts')->toArray(), trans('response.success.groups.add-contacts'));
    }

    public function removeContactsFromGroup(Request $request)
    {
        // Remove contacts from a group for an user
        $input = $request->all();

        // Verify if not logged in
        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.remove-from-groups'), [
                'groups'    => false,
                'message'   => trans('response.errors.users.authenticated')
            ]);
        }

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid',
            'contacts'              => 'required|array',
            'contacts.*'            => 'required|uuid'
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.groups.remove-from-groups'), $validator->errors());
        }

        $user = auth()->user();
        $group = $this->groupRepository->findByUuid($input['uuid']);
        $contacts = $this->contactRepository->findManyByUuid($input['contacts']);

        if (null === $group || null === $contacts
            || !$user->groups->contains($group)
            || !$this->contactRepository->areContactsOwnedByUser($user, $contacts)
            || !$this->groupRepository->areContactsInGroup($group, $contacts)) {
            return $this->sendError(trans('response.errors.groups.remove-from-groups'), [
                'message'   => trans('response.errors.invalid-uuid'),
                'uuid'      => $input['uuid']
            ]);
        }

        // Detach from groupn no deletion
        $group->contacts()->detach($contacts->pluck('id'));

        return $this->sendResponse($group->load('contacts')->toArray(), trans('response.success.groups.remove-from-groups'));
    }
}
