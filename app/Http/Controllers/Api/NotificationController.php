<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\AlertRepositoryInterface;
use App\Contracts\Repositories\NotificationRepositoryInterface;
use Illuminate\Http\Request;
use Validator;

class NotificationController extends BaseController
{
    protected $notificationRepository;

    protected $alertRepository;

    public function __construct(
        NotificationRepositoryInterface $notificationRepository,
        AlertRepositoryInterface $alertRepository
    ) {
        $this->notificationRepository = $notificationRepository;
        $this->alertRepository = $alertRepository;
    }

    public function disableNotification(Request $request)
    {
        $input = $request->all();

        if (!auth()->check()) {
            return $this->sendError(trans('response.errors.notifications.disable'), [
                'notification'   => false,
                'message'        => trans('response.errors.users.authenticated')
            ]);
        }

        $validator = Validator::make($input, [
            'uuid'                  => 'required|uuid', // notification uuid
            'disabled_form'         => 'sometimes|string'
        ]);

        if ($validator->fails()) {
            return $this->sendError(trans('response.errors.notifications.disable'), $validator->errors());
        }

        $notification = $this->notificationRepository->findByUuid($input['uuid']);

        if (null === $notification) {
            return $this->sendError(trans('response.errors.notifications.disable'), [
                'uuid'           => false,
                'message'        => trans('response.errors.users.authenticated')
            ]);
        }

        $disabled_from = (isset($input['disabled_form']) && !empty($input['disabled_from'])) ? $input['disabled_form'] : null;
        $notification->disable($disabled_from);

        return $this->sendResponse($notification->toArray(), trans('response.success.notifications.add'));
    }
}
