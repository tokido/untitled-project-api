<?php

namespace App\CustomCasts;

use Vkovic\LaravelCustomCasts\CustomCastBase;

class EncryptedTextCast extends CustomCastBase
{
    public function setAttribute($value)
    {
        return encrypt($value);
    }

    public function castAttribute($value)
    {
        return decrypt($value);
    }
}
