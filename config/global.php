<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Alert defaults
    |--------------------------------------------------------------------------
    |
    | Controls the default for alert :
    | Receipt is the default configuration of user if he receive receipt if the alert is sent
    |
    */
    'alert' => [
        'receipt'   => [
            'enabled' => true
        ],
        'creatables'   => [
            'alert.types.programmed',
            'alert.types.blackmail'
        ],
        'has_recurrences'    => 'alert.types.blackmail',
        'has_intervals'      => 'alert.types.programmed',
        'emergency'          => [
            'type'      => 'alert.types.emergency',
            'title'     => 'alert.emergency.title',
            'message'   => 'alert.emergency.message'
        ],
        'intrusion' => [
            'type'      => 'alert.types.intrusion',
            'title'     => 'alert.intrusion.title',
            'message'   => 'alert.intrusion.message'
        ],
        'programmables' => [
            'alert.types.programmed'
        ],
        'untimables'    => [
            'alert.types.blackmail'
        ],
        'timeables' => [
            'alert.types.programmed',
            'alert.types.blackmail'
        ],
        'types'     => [
            [
                'name'          => 'alert.types.intrusion',
                'description'   => 'alert.types.intrusion.description',
                'is_creatable'  => false,
                'is_deletable'  => false
            ],
            [
                'name'          => 'alert.types.emergency',
                'description'   => 'alert.types.emergency.description',
                'is_creatable'  => false,
                'is_deletable'  => false
            ],
            [
                'name'          => 'alert.types.programmed',
                'description'   => 'alert.types.programmed.description',
                'is_creatable'  => true,
                'is_deletable'  => true
            ],
            [
                'name'          => 'alert.types.blackmail',
                'description'   => 'alert.types.blackmail.description',
                'is_creatable'  => true,
                'is_deletable'  => true
            ],
        ],
        'days_of_week'  => [0, 1, 2, 3, 4, 5, 6], // Sunday is 0, Monday is 1
        'notifications' => [
            'default_ocurrence' => 3
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Password defaults
    |--------------------------------------------------------------------------
    |
    | Default configurations of password behavior :
    | Receipt is the default configuration of user if he receive receipt if the alert is sent
    |
    */
    'password' => [
        'palyndrome' => [ // if palyndrome is enabled, user can use palyndrome password to send immediate alert
            'enabled'   => env('PASSWORD_PALYNDROME', true)
        ],
        'intrusion'  => [ // send intrusion alert if there is an intrusion attempt
            'enabled'   => env('PASSWORD_INTRUSION', true),
            'timespan'  => 3600,
            'threshold' => 3
        ],
        'two-factor-recovery'  => [ // ask secret question for password recovery
            'enabled'   => env('PASSWORD_TWO_FACT_RECOVERY', true),
            'threshold' => 3,
            'questions' => [
                [
                    'label'        => 'question.favorite-book',
                    'is_enabled'   => true
                ],
                [
                    'label'        => 'question.road-grew-up',
                    'is_enabled'   => true
                ],
                [
                    'label'        => 'question.mothers-maiden-name',
                    'is_enabled'   => true
                ],
                [
                    'label'        => 'question.favorite-pet',
                    'is_enabled'   => true
                ],
                [
                    'label'        => 'question.first-company',
                    'is_enabled'   => true
                ],
                [
                    'label'        => 'question.meet-spouse',
                    'is_enabled'   => true
                ],
                [
                    'label'        => 'question.favorite-food',
                    'is_enabled'   => true
                ],
                [
                    'label'        => 'question.favorite-vacation',
                    'is_enabled'   => true
                ]
            ]
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Pin defaults
    |--------------------------------------------------------------------------
    |
    | Enable or disable pin for easy disabled notifications
    |
    */
    'pin'   => [ // enable pin mode for easy disable notifications
        'enabled'       => env('PIN_ENABLED', true),
        'length'        => 4,
        'palyndrome'    => [
            'enabled'   => env('PIN_PALYNDROME', true)
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Enabled locales
    |--------------------------------------------------------------------------
    |
    | List of enabled locales
    |
    */
    'locales'   => [ // enable pin mode for easy disable notifications
        'enabled'       => ['en_US', 'fr_FR']
    ],

    /*
    |--------------------------------------------------------------------------
    | User related confs
    |--------------------------------------------------------------------------
    |
    | List of enabled locales
    |
    */
    'users'   => [ // enable pin mode for easy disable notifications
        'civility'       => ['civility.mr', 'civility.mrs', 'civility.other'],
        'verification'   => [
            'verification_expire_in'   => 3600
        ],
        'oAuth' => [
            'client_id'          => env('OAUTH_CLIENT_ID', 2),
            'client_secret'      => env('OAUTH_CLIENT_SECRET', 'secret')
        ],
        'parameters'    => [
            'notifiables'   => [
                'email',
                'phone'
            ]
        ],
        'steps' => [
            'identifier_valid'      => 1,
            'secret_question_valid' => 2,
            'emergency_contact'     => 3
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | files
    |--------------------------------------------------------------------------
    |
    | File types on upload
    |
    */
    'files'   => [ // enable pin mode for easy disable notifications
        'types'     => [
            [
                'type'          => 'file.types.local',
                'is_default'    => 1,
                'is_enabled'    => 1
            ],
            [
                'type'          => 'file.types.external',
                'is_default'    => 0,
                'is_enabled'    => 1
            ],
        ],
        'encryption' => [
            'key'       => env('FILE_ENCRYPTION_KEY', 'JMHSJ03ufK7zcy2gMABabbobs7jFQQbAcTbJy3ls'),
            'enabled'   => true,
            'threshold' => (int) env('ENCRYPTION_BLOCKS', 100000) //integer block number to read
        ],
        'upload'    => [
            'max'  => env('FILE_UPLOAD_MAX_SIZE', '100000') //in megabytes
        ],
        'mimes'          => 'jpeg,bmp,png,doc,docx,pdf,txt,md,gif',
        'upload'         => 'file.types.local',
        'paths'          => [
            'temp'      => public_path() . '/temp/',
            'encrypted' => public_path() . '/encrypted/'
        ],
        'download_expiration'   => 2 // In Hours
    ],

    'dates' => [
        'api_input_format'      => 'Y-m-d H:i:s', //'Y-m-d\TH:i:sO',
        'api_output_format'     => 'Y-m-d H:i:s',
        'api_hour_input_format' => 'H:i:s'
    ],

    /*
    |--------------------------------------------------------------------------
    | Admin config
    |--------------------------------------------------------------------------
    |
    | List of config
    |
    */

    'admin' => [
        'email' => env('ADMIN_EMAIL', 'andefasomail@gmail.com')
    ],

    /*
    |--------------------------------------------------------------------------
    | Lang, translation & phone config
    |--------------------------------------------------------------------------
    |
    |
    */
    'i18n'  => [
        'locales'      => [
            'fr_FR' => ['fr_FR' => 'Français', 'en_US' => 'French'],
            'en_US' => ['fr_FR' => 'Anglais (Americain)', 'en_US' => 'English (American)'],
        ],
        'countries' => ['EN', 'FR', 'US', 'MG']
    ]
];
