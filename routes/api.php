<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['domain' => config('app.domain.api')], function () {
    /*
     * User registration process.
     */
    Route::prefix('user')->name('user.')->group(function () {
        // Verify user identifier
        Route::post('exists', [
            'as'    => 'exists',
            'uses'  => 'AuthProcessController@findUserByIdentifier'
        ]);
        // User registration
        Route::post('register', [
            'as'    => 'register',
            'uses'  => 'RegisterController@register'
        ]);
        // Verify user account by email or message
        Route::post('verify-account', [
            'as'    => 'verify',
            'uses'  => 'VerificationApiController@verifyAccount'
        ]);
    });
    /*
    * Offline request file
    */
    Route::prefix('files')->name('files.')->group(function () {
        Route::prefix('download')->name('download.')->group(function () {
            // Download files
            Route::post('/', [
                'as'   => 'zipped',
                'uses' => 'AlertController@downloadZippedFiles'
            ]);
            // Request downloadable file of an alert
            Route::post('request', [
                'as'    => 'request',
                'uses'  => 'AlertController@requestAttachablesDownload'
            ]);
        });
    });
    /*
    * Related to security question
    */
    Route::prefix('questions')->name('questions.')->group(function () {
        Route::get('/', [
            'as'   => 'index',
            'uses' => 'QuestionController@index'
        ]);
    });
    /*
    * Translation list
    */
    Route::get('translations/{locale?}', [
        'as'    => 'translations',
        'uses'  => 'TranslationController@getCurrrent'
    ]);
    /*
    * Logged in users Action
    */
    Route::middleware(['auth:api', 'verified'])->group(function () {
        /*
        * User related actions
        */
        Route::prefix('user')->name('user.')->group(function () {
            // Verify user concact
            Route::post('verify-contact', [
                'as'    => 'verify.contact',
                'uses'  => 'VerificationApiController@verifyContact'
            ]);
            // Add user email
            Route::post('add-email', [
                'as'    => 'add.email',
                'uses'  => 'UserParameterController@addEmail'
            ]);
            // Add user phone
            Route::post('add-phone', [
                'as'    => 'add.phone',
                'uses'  => 'UserParameterController@addPhone'
            ]);
            // Logout from current session
            Route::get('logout', [
                'as'    => 'logout',
                'uses'  => 'AuthProcessController@logout'
            ]);
            // Logout from all Session
            Route::get('logout-all', [
                'as'    => 'logout.all',
                'uses'  => 'AuthProcessController@logoutAll'
            ]);

            /*
            * Related to user contacts
            */
            Route::prefix('contacts')->name('contacts.')->group(function () {
                //Contact list of an user ['with' => 'group']
                Route::get('/', [
                    'as'   => 'index',
                    'uses' => 'ContactController@index'
                ]);
                //Add contact
                Route::post('add', [
                    'as'   => 'add',
                    'uses' => 'ContactController@add'
                ]);
                //Set emergency
                Route::post('set-emergency', [
                    'as'   => 'add',
                    'uses' => 'ContactController@setEmergencyContact'
                ]);
                //Update contact
                Route::post('update', [
                    'as'   => 'update',
                    'uses' => 'ContactController@update'
                ]);
                // Delete one or multiple contact
                Route::post('delete', [
                    'as'   => 'delete',
                    'uses' => 'ContactController@delete'
                ]);
                // Add contact to groups
                Route::post('add-to-groups', [
                    'as'    => 'add-to-groups',
                    'uses'  => 'ContactController@addContactToGroups'
                ]);
                // Remove contact from groups
                Route::post('remove-from-groups', [
                    'as'    => 'remove-from-groups',
                    'uses'  => 'ContactController@removeContactFromGroups'
                ]);
            });

            /*
            * Related to user groups
            */
            Route::prefix('groups')->name('groups.')->group(function () {
                Route::get('/', [
                    'as'   => 'index',
                    'uses' => 'GroupController@index'
                ]);
                //Add group
                Route::post('add', [
                    'as'   => 'add',
                    'uses' => 'GroupController@add'
                ]);
                //Delete group
                Route::post('delete', [
                    'as'   => 'delete',
                    'uses' => 'GroupController@delete'
                ]);
                //rename group
                Route::post('update', [
                    'as'   => 'update',
                    'uses' => 'GroupController@update'
                ]);
                // Add contacts to group
                Route::post('add-contacts', [
                    'as'    => 'add-contacts',
                    'uses'  => 'GroupController@addContactsToGroup'
                ]);
                // Remove contacts to group
                Route::post('remove-contacts', [
                    'as'    => 'remove-contacts',
                    'uses'  => 'GroupController@removeContactsFromGroup'
                ]);
            });

            /*
            * Related to security question
            */
            Route::prefix('questions')->name('questions.')->group(function () {
                // Selected question and answer for user
                Route::post('answer', [
                    'as'   => 'answer',
                    'uses' => 'QuestionController@answer'
                ]);
                // Get current answered question
                Route::get('current', [
                    'as'    => 'current',
                    'uses'  => 'QuestionController@getCurrent'
                ]);
            });

            /*
            * Related to user alerts
            */
            Route::prefix('alerts')->name('alerts.')->group(function () {
                // Get all alerts, ordered by is_enabled - created_at
                Route::get('/', [
                    'as'    => 'index',
                    'uses'  => 'AlertController@index'
                ]);
                // Emergency alert send endpoint
                Route::post('emergency', [
                    'as'    => 'emergency',
                    'uses'  => 'AlertController@emergency'
                ]);
                // Create new alert
                Route::post('add', [
                    'as'    => 'add',
                    'uses'  => 'AlertController@create'
                ]);
                // Update new alert
                Route::post('update', [
                    'as'    => 'update',
                    'uses'  => 'AlertController@update'
                ]);
                //Disable
                Route::post('disable', [
                    'as'    => 'disable',
                    'uses'  => 'AlertController@disable'
                ]);
                //Suspend
                Route::post('suspend', [
                    'as'    => 'suspend',
                    'uses'  => 'AlertController@suspend'
                ]);
                //Enable
                Route::post('enable', [
                    'as'    => 'enable',
                    'uses'  => 'AlertController@enable'
                ]);
                // Set as Draft
                Route::post('draft', [
                    'as'    => 'draft',
                    'uses'  => 'AlertController@draft'
                ]);
                // Delete
                Route::post('delete', [
                    'as'    => 'delete',
                    'uses'  => 'AlertController@delete'
                ]);
                // Get alert Type list
                Route::get('types', [
                    'as'    => 'types',
                    'uses'  => 'AlertController@types'
                ]);
                // Alert Types
                // attachables
                Route::prefix('attachables')->name('attachables.')->group(function () {
                    // Add attachables to an alert
                    Route::post('add', [
                        'as'    => 'add',
                        'uses'  => 'AlertController@addAttachablesToAlert'
                    ]);
                    // Remove attachables to an alert
                    Route::post('remove', [
                        'as'    => 'remove',
                        'uses'  => 'AlertController@removeAttachablesFromAlert'
                    ]);
                });
                // alertables
                Route::prefix('alertables')->name('alertables.')->group(function () {
                    // Add alertables to an alert
                    Route::post('add', [
                        'as'    => 'add',
                        'uses'  => 'AlertController@addAlertablesToAlert'
                    ]);
                    // Remove alertables to an alert
                    Route::post('remove', [
                        'as'    => 'remove',
                        'uses'  => 'AlertController@removeAlertablesFromAlert'
                    ]);
                });
                // disable notification
                Route::prefix('notifications')->name('notifications.')->group(function () {
                    // Disable notification
                    Route::post('disable', [
                        'as'    => 'disable',
                        'uses'  => 'NotificationController@disableNotification'
                    ]);
                });
            });
            /*
            * Related to user folders
            */
            Route::prefix('folders')->name('folders.')->group(function () {
                // Get all folders
                Route::get('/', [
                    'as'    => 'index',
                    'uses'  => 'FolderController@index'
                ]);

                // create folder
                Route::post('add', [
                    'as'    => 'add',
                    'uses'  => 'FolderController@add'
                ]);

                // Delete
                Route::post('delete', [
                    'as'    => 'delete',
                    'uses'  => 'FolderController@delete'
                ]);

                //rename folders
                Route::post('rename', [
                    'as'   => 'rename',
                    'uses' => 'FolderController@rename'
                ]);

                //add files
                Route::post('add-files', [
                    'as'    => 'add-files',
                    'uses'  => 'FolderController@addFilesToFolder'
                ]);

                // Remove contacts to group
                Route::post('remove-files', [
                    'as'    => 'remove-files',
                    'uses'  => 'FolderController@removeFilesFromFolder'
                ]);
            });

            /*
            * Related to user files
            */
            Route::prefix('files')->name('files.')->group(function () {
                Route::get('/', [
                    'as'    => 'index',
                    'uses'  => 'UploadedFileController@index'
                ]);
                Route::post('upload', [
                    'as'   => 'upload',
                    'uses' => 'UploadedFileController@upload'
                ]);
                Route::post('download', [
                    'as'   => 'download',
                    'uses' => 'UploadedFileController@download'
                ]);
            });
        });
    });

    /*
    * 404 route fallback for json errors
    */
    Route::fallback(function () {
        return response()->json([
            'success' => false,
            'message' => 'Not Found.'
        ], 404);
    })->name('api.fallback.404');

    /*
     * Default route
     */
    Route::get('/', function () {
        return response()->json(
            true,
            200
        );
    });
});
