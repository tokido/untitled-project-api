<?php

return [
    /*
    |--------------------------------------------------------------------------
    | API response Language Lines
    |--------------------------------------------------------------------------
    |
    | used as api response
    |
    */

    'index' => [
        'success' => 'Ressource récupérée avec succès.'
    ],

    'register'  => [
        'success' => 'Utilisateur enregistré avec succès.'
    ],

    'generic' => [
        'errors' => [
            'validations' => 'Erreur de validation.'
        ]
    ]
];
