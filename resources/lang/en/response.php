<?php

return [
    /*
    |--------------------------------------------------------------------------
    | API response Language Lines
    |--------------------------------------------------------------------------
    |
    | used as api response
    |
    */
    'index' => [
        'success' => 'Resource retrieved successfully.'
    ],

    'user'  => [
        'register'  => [
            'success' => 'User registered successfully.'
        ]
    ],

    'success' => [
        'alerts'    => [
            'all'   => 'Alerts retrieved',
            'add'   => 'Alert saved'
        ],
        'users' => [
            'exists'        => 'User exists',
            'logout'        => 'Logout success',
            'register'      => 'Registration success',
            'verification'  => 'Verification success'
        ],
        'contacts'  => [
            'all'       => 'Contacts retrived',
            'add'       => 'Contacts insertion success',
            'emergency' => [
                'add'   => 'Emergency contact insertion success'
            ],
            'delete'                => 'Contacts deletion success',
            'update'                => 'Contact update success',
            'to-groups'             => 'Contact addition to group success',
            'remove-from-groups'    => 'Contact removal from groups success'
        ],
        'groups'    => [
            'all'                   => 'Groups retieval success',
            'add'                   => 'Groups insertion success',
            'delete'                => 'Groups deletion success',
            'update'                => 'Groups update success',
            'add-contacts'          => 'Groups addition to group success',
            'remove-from-groups'    => 'Groups removal from groups success'
        ],
        'questions' => [
            'all'       => 'Question retrieval success',
            'answer'    => 'Answer save success',
            'current'   => 'Success'
        ],
        'email' => [
            'add'   => 'Added Email to user'
        ],
        'phone' => [
            'add'   => 'Added Phone to user'
        ]
    ],

    'errors'    => [
        'alerts'    => [
            'all'   => 'Alert retrieval failed',
            'add'   => 'Alert insertion failed'
        ],
        'users' => [
            'exists'            => 'Identifier is invalid, not email nor number',
            'logout'            => 'Logout failed',
            'register'          => 'Registration failed',
            'authenticated'     => 'User is not logged in',
            'verification'      => 'Verification failed',
            'not-verified'      => 'User is not verified',
            'already-verified'  => 'User already verified',
            'pin-expired'       => 'Pin is expired'
        ],
        'contacts'  => [
            'all'       => 'Contacts retrival failed',
            'add'       => 'Contacts insertion failed',
            'emergency' => [
                'add'   => 'Emergency contact insertion failed'
            ],
            'delete'                => 'Contacts deletion failed',
            'update'                => 'Contact update failed',
            'to-groups'             => 'Contact addition to group failed',
            'remove-from-groups'    => 'Contact removal from groups failed'
        ],
        'groups'    => [
            'all'                   => 'Groups retieval failed',
            'add'                   => 'Groups insertion failed',
            'delete'                => 'Groups deletion failed',
            'update'                => 'Groups update failed',
            'add-contacts'          => 'Groups contact addition failed',
            'remove-from-groups'    => 'Groups removal from contact failed'
        ],
        'questions' => [
            'all'       => 'Question retrieval failed',
            'answer'    => 'Answer save failed',
            'current'   => 'Current Question not set'
        ],
        'email' => [
            'add'           => 'Email addition failed',
            'notification'  => 'Email verification notification failed'
        ],
        'phone' => [
            'add'           => 'Phone addition failed',
            'notification'  => 'Phone verification notification failed'
        ],
        'invalid-uuid'  => 'Uuid is invalid, entity doesn\'t exists'
    ]
];
