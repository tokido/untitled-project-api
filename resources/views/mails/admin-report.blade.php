Hello {{$admin}}

A proccess returned a non Zero result at "{{$now}}", please check your log
    - Process : {{$process}}
    - Reported error : {{$reportedError}}
    - Reported classname : {{$reportedClassName}}
    - ClassName Uuid : {{$uuid}}

Thank you, have a good day.

The {{config('app.name')}} team
